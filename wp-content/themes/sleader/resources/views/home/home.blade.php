@php
    if(ICL_LANGUAGE_CODE==en){
        dynamic_sidebar('widget_silder');
    }else{
        dynamic_sidebar('widget_silder_vi');
    }
@endphp
<div id="search_home" class="container-fluid home-input">
    <div class="container">
        <div class="col-xs-12 search_header">

            @php
                get_search_form();
            @endphp
           
        </div>
    </div>
</div>
<div class="home">
    <div class="noibat-home">
        <div class="container" style="margin-top: 47px;">
            <div class="row">
                <div class="col-xs-12">
                    <h4 class="pull-left title_comlum"><p><?php echo __('Highlights', 'homepage'); ?></p></h4>
                </div>
            </div>
            <div class="row list_noibat">
                
                @php
                    $noibat_page = new WP_Query(array('post_type'=>'page','posts_per_page'=>-1,));

                    foreach ($noibat_page->posts as $key => $post) {
                        $order = get_field('order', $post->ID);
                        $post->order = $order;
                    }

                    usort($noibat_page->posts, function($a, $b) {
                        return ($a->order <= $b->order);
                    });

                    foreach ($noibat_page->posts as $key => $value) :

                    $id = $value->ID;


                    $noibat1 = get_field('noi_bat_home', $id);

                    $img = wp_get_attachment_url(get_post_meta( $id, '_thumbnail_id', true ));
                    $link_page = get_permalink($id);
                    $title_page = get_the_title($id);

                    if($noibat1 == "co"){
                @endphp

                        <div class="col-xs-6 col-sm-3 recomm">
                            <a href="{{ $link_page }}" class="url_noibat">
                                <div class="thumbnail">
                                    <img style="background-image: url({{ $img }});" src="{{ get_stylesheet_directory_uri() }}/resources/assets/images/home/post.png" />
                                </div>
                                <div class="summary">
                                    <h5>
                                        @php echo $title_page; @endphp
                                    </h5>
                                </div>
                            </a>
                        </div>

                @php
                        }
                    endforeach;
                @endphp
            </div>
        </div>
    </div>
</div>
<div class="home" style="border-bottom: 20px solid #fff;">
    <div class="khoahoc_news">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12 colum1">
                    <div class="list_kh">
                        <ul class="list_post">
                            <li style="background: #E1E1E1;" class="title_danhmuc">
                                <?php echo __('Courses', 'homepage'); ?>
                            </li>

                            <div class="hide_ajaxshow_courses">

                            @php
                                add_action('pre_get_posts', function ($query) {
                                if($query->query['post_type']  == 'khoahoc' && is_home()) {
                                        $query->set('posts_per_page', 3);
                                    }
                                });

                                $args_kh = [
                                    'post_type' => 'khoahoc', 
                                    'posts_per_page' => 3,
                                    'paged' => 1
                                ];

                                $khoahoc_home = new WP_Query($args_kh);

                                foreach ($khoahoc_home->posts as $key_khoahoc => $value_khoahoc) :

                                $id_kh = $value_khoahoc->ID;

                                $img_kh = wp_get_attachment_url(get_post_meta( $id_kh, '_thumbnail_id', true ));
                                $link_single_kh = get_permalink($id_kh);

                                $title_page_kh = get_the_title($id_kh);
                                $date_news = get_post_time( 'l, d-F-Y', false, $id_kh, 'vi' );
                                
                            @endphp

                                <li>
                                    <div class="row">
                                        <a href="{{ $link_single_kh }}">
                                            <div class="col-md-5 images_kh">
                                                <img style="background-image: url({{ $img_kh }});" src="{{ get_stylesheet_directory_uri() }}/resources/assets/images/home/post.png" />
                                            </div>

                                            <div class="col-md-7 single_home">
                                                <p class="title_kh_home">@php echo $title_page_kh; @endphp</p>

                                                <p class="time_kh">@php echo $date_news; @endphp</p>
                                            </div>
                                        </a>
                                    </div>
                                </li>

                            @php
                                endforeach;
                            @endphp
                                </div>
                                <div class="show_ajaxpost_courses"></div>
                                <p class="list_khoahoc left_courses">
                                    <i class="fa fa-spinner fa-pulse fa-2x fa-fw spinner-loading hide"></i>
                                    <a href="javascript:void(0)" id="apipost_courses_prev" currpage="1"><i class="fa fa-chevron-left" aria-hidden="true"></i> <?php echo __('Previous news', 'homepage'); ?></a>
                                </p>
                                <p class="list_khoahoc right_courses">
                                    <i class="fa fa-spinner fa-pulse fa-2x fa-fw spinner-loading hide"></i>
                                    <a href="javascript:void(0)" id="apipost_courses_next" currpage="1"><?php echo __('Next news', 'homepage'); ?> <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                                </p>
                        </ul>
                    </div>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 colum2">
                    <ul class="list_post">
                        <li style="background: #E1E1E1;" class="title_danhmuc"><?php echo __('Spotlights', 'homepage'); ?></li>
                        <div class="hide_ajaxshow"> 
                        @php
                            // $news_nb = new WP_Query(array('post_type'=>'news','posts_per_page'=>3,));

                            //wp_reset_query();
                            //wp_reset_postdata();
                            //set(get_query_var('posts_per_page', 3);

                            add_action('pre_get_posts', function ($query) {
                                if($query->query['post_type']  == 'news') {
                                    $query->set('posts_per_page', 3);
                                }
                            });

                            $args = [
                                'post_type' => 'news', 
                                'posts_per_page' => 3,
                                'paged' => 1
                            ];
                            $news_nb = new WP_Query($args);

                            foreach ($news_nb->posts as $key_news => $value_news) :

                            $id_news = $value_news->ID;

                            $img_news = wp_get_attachment_url(get_post_meta( $id_news, '_thumbnail_id', true ));
                            $link_single_news = get_permalink($id_news);

                            $title_page_news = get_the_title($id_news);

                            $date_news = get_post_time( 'l, d-F-Y', false, $id_news, 'vi' );
                        @endphp

                            <li>
                                <div class="row">
                                    <a href="{{ $link_single_news }}">
                                        <div class="col-md-5 images_kh">
                                            <img style="background-image: url({{ $img_news }});" src="{{ get_stylesheet_directory_uri() }}/resources/assets/images/home/post.png" />
                                        </div>

                                        <div class="col-md-7 single_home">
                                            <p class="title_kh_home">@php echo $title_page_news; @endphp</p>

                                            <p class="time_kh">@php echo $date_news; @endphp</p>
                                        </div>
                                    </a>
                                </div>
                            </li>


                        @php
                            endforeach;
                        @endphp
                        </div>
                        <div class="show_ajaxpost"></div>
                        <p class="list_khoahoc left">
                            <i class="fa fa-spinner fa-pulse fa-2x fa-fw spinner-loading hide"></i>
                            <a href="javascript:void(0)" id="apipost_highlight_prev" currpage="1"><i class="fa fa-chevron-left" aria-hidden="true"></i> <?php echo __('Previous news', 'homepage'); ?></a>
                        </p>
                        <p class="list_khoahoc right">
                            <i class="fa fa-spinner fa-pulse fa-2x fa-fw spinner-loading hide"></i>
                            <a href="javascript:void(0)" id="apipost_highlight_next" currpage="1"><?php echo __('Next news', 'homepage'); ?> <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                        </p>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="home">
    <div class="khoahoc_news">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12 colum1">
                    <div class="list_kh">
                        <ul class="list_post">
                            <li style="background: #E1E1E1;" class="title_danhmuc"><?php echo __('Sharing knowledge', 'homepage'); ?></li>
                            <div class="hide_ajaxshow_share"> 
                            @php

                                add_action('pre_get_posts', function ($query) {
                                    if($query->query['post_type']  == 'wpdmpro') {
                                        $query->set('posts_per_page', 3);
                                    }
                                });

                                $args = [
                                    'post_type' => 'wpdmpro', 
                                    'posts_per_page' => 3,
                                    'tax_query' => array(
                                        array(
                                          'taxonomy' => 'wpdmcategory',
                                          'field'    => 'slug',
                                          'slug'    => 'cac-an-pham-khac',
                                          'operator'         => 'AND',
                                          'include_children' => false,
                                        ),
                                    ),
                                    'paged' => 1
                                ];
                                $news_nb = new WP_Query($args);

                                foreach ($news_nb->posts as $key_news => $value_news) :

                                $id_news = $value_news->ID;

                                $img_news = wp_get_attachment_url(get_post_meta( $id_news, '_thumbnail_id', true ));
                                $link_single_news = get_permalink($id_news);

                                $title_page_news = get_the_title($id_news);

                                $date_news = get_post_time( 'l, d-F-Y', false, $id_news, 'vi' );
                            @endphp

                                <li>
                                    <div class="row">
                                        <a href="{{ $link_single_news }}">
                                            <div class="col-md-5 images_kh">
                                                <img style="background-image: url({{ $img_news }});" src="{{ get_stylesheet_directory_uri() }}/resources/assets/images/home/post.png" />
                                            </div>

                                            <div class="col-md-7 single_home">
                                                <p class="title_kh_home">@php echo $title_page_news; @endphp</p>

                                                <p class="time_kh">@php echo $date_news; @endphp</p>
                                            </div>
                                        </a>
                                    </div>
                                </li>


                            @php
                                endforeach;
                            @endphp
                            </div>
                            <div class="show_ajaxpost_share"></div>
                            <p class="list_khoahoc left_share">
                                <i class="fa fa-spinner fa-pulse fa-2x fa-fw spinner-loading hide"></i>
                                <a href="javascript:void(0)" id="apipost_share_prev" currpage="1"><i class="fa fa-chevron-left" aria-hidden="true"></i> <?php echo __('Previous news', 'homepage'); ?></a>
                            </p>
                            <p class="list_khoahoc right_share">
                                <i class="fa fa-spinner fa-pulse fa-2x fa-fw spinner-loading hide"></i>
                                <a href="javascript:void(0)" id="apipost_share_next" currpage="1"><?php echo __('Next news', 'homepage'); ?> <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                            </p>
                        </ul>
                    </div>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 colum2">
                    <ul class="list_post">
                        <li style="background: #E1E1E1;" class="title_danhmuc"><?php echo __('Point of view', 'homepage'); ?></li>
                        <div class="hide_ajaxshow_khoahoc">
                        @php

                            add_action('pre_get_posts', function ($query) {
                            if($query->query['post_type']  == 'point-of-view' && is_home()) {
                                    $query->set('posts_per_page', 3);
                                }
                            });

                            $args_gn = [
                                'post_type' => 'point-of-view', 
                                'posts_per_page' => 3,
                                'paged' => 1
                            ];

                            $gocnhin_home = new WP_Query($args_gn);

                            foreach ($gocnhin_home->posts as $key_gocnhin => $value_gocnhin) :

                            $id_kh = $value_gocnhin->ID;

                            $img_kh = wp_get_attachment_url(get_post_meta( $id_kh, '_thumbnail_id', true ));
                            $link_single_kh = get_permalink($id_kh);

                            $title_page_kh = get_the_title($id_kh);
                            $date_news = get_post_time( 'l, d-F-Y', false, $id_kh, 'vi' );
                        @endphp

                            <li>
                                <div class="row">
                                    <a href="{{ $link_single_kh }}">
                                        <div class="col-md-5 images_kh">
                                            <img style="background-image: url({{ $img_kh }});" src="{{ get_stylesheet_directory_uri() }}/resources/assets/images/home/post.png" />
                                        </div>

                                        <div class="col-md-7 single_home">
                                            <p class="title_kh_home">@php echo $title_page_kh; @endphp</p>

                                            <p class="time_kh">@php echo $date_news; @endphp</p>
                                        </div>
                                    </a>
                                </div>
                            </li>

                        @php
                            endforeach;
                        @endphp
                            </div>
                            <div class="show_ajaxpost_kh"></div>
                            <p class="list_khoahoc left_kh">
                                <i class="fa fa-spinner fa-pulse fa-2x fa-fw spinner-loading hide"></i>
                                <a href="javascript:void(0)" id="apipost_highlight_prev_kh" currpage="1"><i class="fa fa-chevron-left" aria-hidden="true"></i> <?php echo __('Previous news', 'homepage'); ?></a>
                            </p>
                            <p class="list_khoahoc right_kh">
                                <i class="fa fa-spinner fa-pulse fa-2x fa-fw spinner-loading hide"></i>
                                <a href="javascript:void(0)" id="apipost_highlight_next_kh" currpage="1"><?php echo __('Next news', 'homepage'); ?> <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                            </p>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
