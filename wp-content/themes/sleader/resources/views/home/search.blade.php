<div class="search">
    <div class="container">
        @php
            global $query_string;
            $query_args = explode("&", $query_string);
            $search_query = array();

            foreach($query_args as $key => $string) {
              $query_split = explode("=", $string);
              $search_query[$query_split[0]] = urldecode($query_split[1]);
            } // foreach

            $the_query = new WP_Query($search_query);

            echo "<h2 class='title_tk' style='font-weight:bold;color:#000'>" . __('Search Results:', 'search') . get_query_var('s')."</h2>";

            if ( $the_query->have_posts() ) : 
            @endphp
            <!-- the loop -->

            <ul class="list_tk col-md-9">    
            @php

                while ( $the_query->have_posts() ) : $the_query->the_post();

                $images = wp_get_attachment_url(get_post_thumbnail_id($post->ID));

                $link = get_permalink($post->ID);

                $mota = get_the_excerpt($post->ID);
                // var_dump($images);
                // exit();
            @endphp
                <li class="row">
                    <a href="{{ $link }}">
                        <div class="images col-md-3">
                            <img style="background-image: url({{ $images }});" src="{{ get_stylesheet_directory_uri() }}/resources/assets/images/home/post.png" />
                        </div>

                        <div class="title col-md-9">
                            <p class="title_post">@php the_title(); @endphp</p>
                            <p class="mota">@php echo $mota; @endphp</p>
                            <p><?php echo __('See more', 'search'); ?> <i class="fa fa-angle-double-right" aria-hidden="true"></i></p>
                        </div>
                    </a>
                </li>   
            @php endwhile; @endphp
            </ul>
            <!-- end of the loop -->

            @php wp_reset_postdata(); @endphp

        @php else : @endphp
            <p class="no_timkiem">@php echo __( 'Sorry, no posts matched your criteria.' , 'search'); @endphp</p>
        @php endif; @endphp
    </div>
</div>