<form role="search" method="get" id="searchform" action="@php echo home_url('/'); @endphp">
	<div>
		<i class="fa fa-search" aria-hidden="true"></i>
		<input class="timkiem" type="text" value="" name="s" id="s" placeholder="@php echo __('Enter search content ...', 'header'); @endphp"><input class="submit_tk" type="submit" id="searchsubmit" value="@php echo __('Search', 'header'); @endphp">
	</div>
</form>