<li class="row">
	<a href="{{ $url }}">
		<div class="col-md-4 col-sm-4 image_news">
			<img style="background-image: url({{ $thumbnail }});" src="{{ get_stylesheet_directory_uri() }}/resources/assets/images/khoahoc/list_khoahoc.png" alt="">
		</div>

		<div class="col-md-8 col-sm-8">
			<p class="title_news">{{ $title }}</p>

			<p class="date_news">{{ $date }}</p>
		</div>
	</a>
</li>