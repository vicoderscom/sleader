<div class="container list_news">
	<div class="row">
		<div class="col-md-9 filter_khoahoc">
			<div class="news">
				@php
					echo do_shortcode('[news-sukien post_type="news" filter="yes" per_page="9" paged="yes"]');
				@endphp
			</div>
		</div>

		<div class="col-md-3 sidebar">
			<div class="sidebar_khoahoc">
				<h4 class="aside-title">
		            @php
		            	echo __('Course', 'sidebar-sleader');
		            @endphp
		            <a href="@php
		            	if(ICL_LANGUAGE_CODE==en){
		            		echo site_url('en'). '/lich-khoa-hoc';
		            	}else{
		            		echo site_url('vi'). '/lich-khoa-hoc';
		            	}
		            @endphp">@php echo __('See All Course', 'sidebar-sleader'); @endphp</a>
		        </h4>

		        <div class="list_kh_sidebar">
		        	<ul>
		        		@php
		        			$khoahoc_sidebar = array(
								'post_type'      => 'khoahoc',
								'posts_per_page' => 5,
							);

							$list_kh = new WP_Query($khoahoc_sidebar);

							while ( $list_kh->have_posts() ) : $list_kh->the_post();

							$ngay_bat_dau = get_field('ngay_bat_dau', $post->ID);

                            $ngay_ket_thuc = get_field('ngay_ket_thuc', $post->ID);

							$img_kh =wp_get_attachment_url(get_post_thumbnail_id($post->ID));

	                        $link_kh = get_permalink($post->ID);

	                        $title_kh = get_the_title($post->ID);

	                        $single_mobile = get_the_excerpt();

		        		@endphp
			        		<li class="row">
			        			<a href="{{ $link_kh }}">
			        				<div class="col-md-4 col-sm-4 images_khoahoc">
				        				<img style="background-image: url({{ $img_kh }});" src="{{ get_stylesheet_directory_uri() }}/resources/assets/images/sidebar/events.png" alt="">
				        			</div>

									<div class="col-md-8 col-sm-8">
										<p class="title_kh">@php echo $title_kh; @endphp</p>
										
										<p class="single_mobile">{{ $single_mobile }}</p>

										<p class="time_date">@php echo $ngay_bat_dau; @endphp - @php echo $ngay_ket_thuc; @endphp</p>
									</div>
			        			</a>
			        		</li>

		        		@php
							endwhile; wp_reset_query();
						@endphp
		        	</ul>
		        </div>
			</div>
		</div>
	</div>
</div>