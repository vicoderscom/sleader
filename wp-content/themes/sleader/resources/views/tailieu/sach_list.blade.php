<div class="list_book">
	<div class="row">
		@php

			while ($query->have_posts()) {
            $query->the_post();

			$img_book = wp_get_attachment_url(get_post_thumbnail_id($post->ID));

            $link_book = get_permalink($post->ID);

            $title_book = get_the_title($post->ID);

            $mota_book = get_the_excerpt($post->ID);
		@endphp

			<div class="col-md-4 col-sm-6">
				<div class="book">
						<a href="{{ $link_book }}">
						<div class="images_book">
							<img style="background-image: url({{ $img_book }});" src="{{ get_stylesheet_directory_uri() }}/resources/assets/images/book/book.png" alt="">
						</div>

						<p class="title_book">{{ $title_book }}</p>

						<p class="mota_book">{{ $mota_book }}</p>

						<div class="icon_download">
							<img src="{{ get_stylesheet_directory_uri() }}/resources/assets/images/book/icon_down.png" alt="">
						</div>
					</a>	
				</div>
			</div>
			
		@php
        	}
    	@endphp

	</div>
</div>