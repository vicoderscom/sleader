<div class="page_book">
	<div class="container">
		@php
			if (have_posts()){

				while (have_posts()) {
					the_post();

					the_content();
				}
			}
		@endphp
	</div>
</div>