<!DOCTYPE html>
<html style="margin-top:0 !important">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        @if(is_home())
            {!! 'Trang chủ' !!}
        @endif
        @php
            wp_title(''); 
            @endphp
    </title>

    <link rel="stylesheet" href="<?php echo bloginfo('template_directory');?>/resources/assets/styles/layouts/animate.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    <script src='https://www.google.com/recaptcha/api.js'></script>
    @php wp_head(); @endphp
</head>

<body>
    {{-- <div class="wraper home"> --}}
        <header class="header-section nav-collapsed">
            <!-- TopBar -->
            <div class="topBar @php if(current_user_can('administrator')){ echo "amdin_header"; } @endphp">
                <div class="container">
                    <div class="member_online">
                        <span class="time_h">
                            <i class="fa fa-clock-o" aria-hidden="true"></i> <?php 
                            echo date_i18n("H:i"); 
                            ?>
                        </span>

                        <span class="time_d">@php echo date_i18n('F j,Y'); @endphp</span>

                        <span class="user_o"><i class="fa fa-eye" aria-hidden="true"></i> <?php echo do_shortcode('[wpstatistics stat=visitors time=total]'); ?></span>

                        <span class="total"><i class="fa fa-user" aria-hidden="true"></i> <?php echo do_shortcode('[wpstatistics stat=usersonline]'); ?> online</span>
                    </div>
                    <nav class="topNav">
                        <ul class="@php if(ICL_LANGUAGE_CODE==vi){echo'menuheader_mobile';}else{echo'menuheader_mobile_en';} @endphp">
                            @php
                                wp_nav_menu(
                                    [ 
                                        'menu' => 'Menu-header', 
                                        // 'menu_id' => 'menu_header', 
                                        // 'menu_class' => 'nav navbar-nav', 
                                        'container' => ''
                                    ]
                                );
                            @endphp
                            @php
                                if(ICL_LANGUAGE_CODE==en){
                                    dynamic_sidebar('widget_icon_header');
                                }else{
                                    dynamic_sidebar('widget_icon_header_vi');
                                }

                            // dynamic_sidebar('widget_icon_header');

                            @endphp

                            @php
                                do_action('wpml_add_language_selector');
                            @endphp
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="cLr"></div>

            <div class="mobile_total">
                <span class="time_h">
                    <i class="fa fa-clock-o" aria-hidden="true"></i> <?php 
                    echo date_i18n("H:i"); 
                    ?>
                </span>

                <span class="time_d">@php echo date_i18n('F j,Y'); @endphp</span>

                <span class="user_o"><i class="fa fa-eye" aria-hidden="true"></i> <?php echo do_shortcode('[wpstatistics stat=visitors time=total]'); ?></span>

                <span class="total"><i class="fa fa-user" aria-hidden="true"></i> <?php echo do_shortcode('[wpstatistics stat=usersonline]'); ?> online</span>
            </div>

            <!-- /TopBar -->
            <div class="header-inner">
                <div class="container">
                    <div>
                        
                        <div class="logo">

                            @php
                                $customLogoId = get_theme_mod('custom_logo');
                                $logo = wp_get_attachment_image_src($customLogoId , 'full');
                            @endphp
                            <a href="@php
                                    if(ICL_LANGUAGE_CODE==en){
                                        echo site_url('en');
                                    }else{
                                        echo site_url();
                                    }
                                @endphp" title="home" style="text-decoration: none;">
                                    
                                    <img class="img-responsive" src="{{ $logo[0] }}" alt="">

                                @php
                                    if(ICL_LANGUAGE_CODE==en){
                                        dynamic_sidebar('widget_logo');
                                    }else{

                                        dynamic_sidebar('widget_logo_vi');
                                    }
                                @endphp

                            </a>
                        </div>
                    </div>
                    <!-- MainNav -->
                </div>
            </div>

            <div class="menu @if(is_home())menu_home @endif">
                <div class="border_menu">
                    
                </div>

                <div class="container">
                    <nav id="myNavbar" class="navbar supper_menu">
                        @php
                            wp_nav_menu(
                                [ 
                                    'menu' => 'Menu-chinh', 
                                    //'menu_id' => 'myNavbar', 
                                    'menu_class' => 'nav navbar-nav menu_primary home_submu', 
                                    'container' => ''
                                ]
                            );
                        @endphp
                    </nav>

                    <div class="mobile-menu"></div>
                    <!-- /MainNav -->
                    <!-- Masthead -->
                    <ul class="nav navbar-nav navbar-right menu-login @php if(current_user_can('administrator')){ echo "amdin_header"; } @endphp"">
                        <li class="login">
                            <a href="@php
                                        if(ICL_LANGUAGE_CODE==en){
                                            echo site_url('en'). '/login';
                                        }else{
                                            echo site_url('vi'). '/login';
                                        }
                                    @endphp" class="btn btn-red">@php echo __('Login', 'header'); @endphp</a>
                        </li>
                        <li>
                            @php
                                if(is_home()){
                                    echo '<a class="search-head"><i class="fa fa-search" aria-hidden="true"></i></a>';
                                }else{
                                    echo '<a id="page_id"><i class="fa fa-search" aria-hidden="true"></i></a>';
                                }

                            @endphp
                        </li>
                    </ul>

                </div>
            </div>
        </header>
        @php
            submenu_nav_item();
        @endphp

        @if (!is_home())
            <div id="search_page_item" class="container-fluid home-input search_page">
                <div class="container">
                    <div class="col-xs-12 search_header">

                        @php
                            get_search_form();
                        @endphp
                       
                    </div>
                </div>
            </div>
        @endif
        

        @php
            if(ICL_LANGUAGE_CODE==en){
                dynamic_sidebar('widget_hotline');
            }else{
                dynamic_sidebar('widget_hotline_vi');
            }
        // dynamic_sidebar('widget_chien_luoc');

        @endphp