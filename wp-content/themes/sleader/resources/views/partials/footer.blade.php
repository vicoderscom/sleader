<div style="background:#e8e8e3;height: 20px;"></div>
</div>
<footer>
    <div class="footer">
        @php
            if(ICL_LANGUAGE_CODE==en){
                dynamic_sidebar('widget_footer');
            }else{
                dynamic_sidebar('widget_footer_vi');
            }
        @endphp
    </div>
</footer>
{{-- </div> --}}

@php
    if(current_user_can('administrator')){
    	echo "<div class='admin_supper'>";
        	wp_footer();
        echo "</div>";
    }else{
        echo "<div class='admin'>";
        	wp_footer();
        echo "</div>";
    }
@endphp

{{-- @php wp_footer(); @endphp --}}
</body>

</html>
