<div class="single_book">
	<div class="container" style="margin-top: 40px;">
		<div class="row">
			<div class="col-md-9 col-xs-12 filter_khoahoc">
				@php
					if (have_posts()){

					while (have_posts()) :
						the_post();

					$xemtruoc = get_field('link_xem_truoc', $post->ID);
				@endphp
				<h3 class="title_single" style="margin-top: 0px;">
					@php
						the_title();
					@endphp
				</h3>

				<div class="intrinsic-container" style="width: 100%;">
					{{-- <iframe id="ifr" name="ifr" src="{{ $xemtruoc }}" style="width:100%; height:500px;" frameborder="0"></iframe> --}}

					{{-- <iframe src="http://docs.google.com/gview?url={{ $xemtruoc }}&embedded=true" style="width:600px; height:500px;" frameborder="0"></iframe> --}}

					{{-- <embed src="{{ $xemtruoc }}#toolbar=0&navpanes=0&scrollbar=0" style="width:100%; height:500px;"> --}}
				
					{{-- <iframe src="{{ $xemtruoc }}#toolbar=0" style="width:100%; height:500px;"></iframe> --}}

					{{-- <iframe src="{{ $xemtruoc }}#toolbar=0" style="width:100%; height:500px;"></iframe> --}}

					{{-- <iframe src="http://docs.google.com/gview?url={{ $xemtruoc }}&embedded=true" style="width:100%; height:500px;" frameborder="0"></iframe> --}}
					
					<iframe src="http://docs.google.com/gview?url={{ $xemtruoc }}&embedded=true" style="width:100%; height:500px;" frameborder="0"></iframe>

				</div>

				<div class="content_single">
					@php
						the_content();
						$package = wpdm_get_package(get_the_ID());

						$upload_dir = wp_upload_dir();
						$base_upload = $upload_dir['baseurl'];

						$user_roles = wpdm_user_has_access(get_the_ID());

						// echo "<pre>";
						// var_dump($package);

						$url_single_dl = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

						// echo "<pre>";
						// var_dump($_SERVER); die;

					@endphp
					
					<div class="download">
						@php
							$login = is_user_logged_in();
						@endphp
						<p class="mota_wpdl">@php echo __('Please Download', 'single-book'); @endphp <br>@php
							echo __('to view full documentation', 'single-book');
						@endphp</p>
						<p class="nut_download"><a href="@php
							if($login == true){
								echo $package['download_url'];
							}else{
								echo site_url('login') . '?redirect_to=' . $url_single_dl . '';
							}
						@endphp"><i class="fa fa-download" aria-hidden="true"></i> @php
							echo __('download', 'single-book');
						@endphp</a></p>
					</div>
				</div>

				@php
					endwhile;
					}
				@endphp
			</div>

			<div class="col-md-3 col-xs-12 sidebar">
				<div class="sidebar_khoahoc">
					<h4 class="aside-title">
			            @php
			            	echo __('Course', 'sidebar-sleader');
			            @endphp
			            <a href="@php
			            	if(ICL_LANGUAGE_CODE==en){
			            		echo site_url('en'). '/lich-khoa-hoc';
			            	}else{
			            		echo site_url('vi'). '/lich-khoa-hoc';
			            	}
			            @endphp">@php echo __('See All Course', 'sidebar-sleader'); @endphp</a>
		        	</h4>

			        <div class="list_kh_sidebar">
			        	<ul>
			        		@php
			        			$sidebar_book = array(
									'post_type'      => 'khoahoc',
									'posts_per_page' => 5,
								);

								$list_sidebar_book = new WP_Query($sidebar_book);

								while ( $list_sidebar_book->have_posts() ) : $list_sidebar_book->the_post();

								$img_book =wp_get_attachment_url(get_post_thumbnail_id($post->ID));

		                        $link_book = get_permalink($post->ID);

		                        $title_book = get_the_title($post->ID);

		                        $date_news = get_post_time( 'l, d-F-Y', false, $id_news, 'vi' );

		                        $single_mobile = get_the_excerpt();

			        		@endphp
				        		<li class="row">
				        			<a href="{{ $link_book }}">
				        				<div class="col-md-4 col-sm-4 images_khoahoc">
					        				<img style="background-image: url({{ $img_book }});" src="{{ get_stylesheet_directory_uri() }}/resources/assets/images/sidebar/events.png" alt="">
					        			</div>

										<div class="col-md-8 col-sm-8">
											<p class="title_kh">@php echo $title_book; @endphp</p>

											<p class="single_mobile">{{ $single_mobile }}</p>

											<p class="time_date">{{ $date_news }}</p>
										</div>
				        			</a>
				        		</li>

			        		@php
								endwhile; wp_reset_query();
							@endphp
			        	</ul>
			        </div>
				</div>
			</div>
		</div>
	</div>
</div>