<div class="formdangky">
	<div class="container">
		@php
            while (have_posts()) {
                the_post();

                the_content();
            }
    	@endphp
	</div>
</div>