<div class="detail_about" style="margin-top: 35px;">
	<div class="container">

		@php
			if (have_posts()){

			while (have_posts()) :
				the_post();
		@endphp
		<h3 class="title_single" style="margin-top: 0px;">
			@php
				the_title();
			@endphp
		</h3>

		<div class="content_single">
			@php
				the_content();
			@endphp
		</div>

		@php
			endwhile;
			}
		@endphp
	</div>
</div>