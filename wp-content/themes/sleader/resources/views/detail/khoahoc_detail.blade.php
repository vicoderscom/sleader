<div class="detail_khoahoc">
    <div class="container" style="margin-top: 15px;">
        <div class="col-md-9 filter_khoahoc">
        @php
            if (have_posts()) :

            while (have_posts()) :
                the_post();

            //$diadiem = get_field('dia_diem', $post->ID);

            $ngaybatdau = get_field('ngay_bat_dau', $post->ID);

            $ngayketthuc = get_field('ngay_ket_thuc', $post->ID);

            $status = get_field('trang_thai', $post->ID);

            $urlmaps = get_field('google_maps', $post->ID);

            $time_star = get_field('start_time', $post->ID);

            $time_finish = get_field('finish_time', $post->ID);

            $money_members = get_field('money_members', $post->ID);

            $money_no_members = get_field('money_no_members', $post->ID);

            $time_start_hoc = get_field('time_bat_dau_hoc', $post->ID);

            $location = wp_get_post_terms(get_the_ID(), 'categoty_location');

            $thoi_luong = get_field('thoi_luong', $post->ID);

                
            if (!empty($location)) {
                
                $locations = [];

                foreach ($location as $key => $value) {
                    $locations[] = $value->name;
                }

                $locationStr = implode(', ', $locations);

            }

            //var_dump($locationStr);
        @endphp
            <div class="form_thongtin">
                <div class="form_thongtin">
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <p class="icon_mxh">
                                @php echo __('SHARE THIS', 'single-khoahoc'); @endphp
                            </p>

                            @php echo do_shortcode('[addtoany]'); @endphp

                            <div class="thoi_luong">
                                @php echo __('Time: ', 'single-khoahoc');@endphp 
                                <span>{{ $thoi_luong }}</span>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6" style="text-align: center;">
                            <p class="dangky"><a href="#contact_form_pop" class="fancybox">@php echo __('REGISTER THE COURSE', 'single-khoahoc'); @endphp</a></p>

                            <p class="dangky_mobile"><a href="@php
                                    if(ICL_LANGUAGE_CODE==en){
                                        echo site_url('en'). '/course-registration-form';
                                    }else{
                                        echo site_url('vi'). '/course-registration-form';
                                    }
                                @endphp">@php echo __('REGISTER THE COURSE', 'single-khoahoc'); @endphp</a></p>

                            <div style="display:none" class="fancybox-hidden">
                                <div id="contact_form_pop">
                                    @php
                                        if(ICL_LANGUAGE_CODE==en){
                                            echo do_shortcode('[contact-form-7 id="555" title="Đăng ký khóa học tiếng anh"]');
                                        }else{
                                            echo do_shortcode('[contact-form-7 id="185" title="Contact form 1"]');
                                        }
                                     @endphp
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <h3 class="title_single">
                @php
                    the_title();
                @endphp
            </h3>

            <div class="content_khoahoc">
                @php
                    the_content();
                @endphp
            </div>
            
        @php
            endwhile;
            endif;
        @endphp
        </div>
        <div class="col-md-3 sidebar">
            <div class="sidebar_khoahoc">
                {{-- <a href="{{ site_url('lich-khoa-hoc') }}" style="text-decoration: none; color: #333;"> --}}
                    <h4 class="aside-title">
                    @php
                        echo __('Course', 'sidebar-sleader');
                    @endphp
                    <a href="@php
                        if(ICL_LANGUAGE_CODE==en){
                            echo site_url('en'). '/lich-khoa-hoc';
                        }else{
                            echo site_url('vi'). '/lich-khoa-hoc';
                        }
                    @endphp">@php echo __('See All Course', 'sidebar-sleader'); @endphp</a>
                </h4>
                {{-- </a> --}}
                <div class="list_kh_sidebar">
                    <ul>
                        @php
                            $khoahoc_sidebar = array(
                                'post_type'      => 'khoahoc',
                                'posts_per_page' => 5,
                            );

                            $list_kh = new WP_Query($khoahoc_sidebar);

                            while ( $list_kh->have_posts() ) : $list_kh->the_post();

                            $ngay_bat_dau = get_field('ngay_bat_dau', $post->ID);

                            $ngay_ket_thuc = get_field('ngay_ket_thuc', $post->ID);

                            $img_kh =wp_get_attachment_url(get_post_thumbnail_id($post->ID));

                            $link_kh = get_permalink($post->ID);

                            $title_kh = get_the_title($post->ID);

                            $single_mobile = get_the_excerpt();

                            $location = wp_get_post_terms(get_the_ID(), 'categoty_location');

                            $thoi_luong_2 = get_field('thoi_luong', $post->ID);

                            if (!empty($location)) {
                
                                $locations = [];

                                foreach ($location as $key => $value) {
                                    $locations[] = $value->name;
                                }

                                $locationStr = implode(', ', $locations);
                                
                            }

                            //var_dump($locationStr);
                        @endphp
                            <li class="row">
                                <a href="{{ $link_kh }}">
                                    <div class="col-md-4 col-sm-4 images_khoahoc">
                                        <img style="background-image: url({{ $img_kh }});" src="{{ get_stylesheet_directory_uri() }}/resources/assets/images/sidebar/events.png" alt="">
                                    </div>

                                    <div class="col-md-8 col-sm-8">
                                        <p class="title_kh">@php echo $title_kh; @endphp</p>
                                        
                                        <p class="single_mobile">{{ $single_mobile }}</p>

                                        <p class="time_date">@php echo __('Time:', 'daotao'); @endphp {{ $thoi_luong_2 }}</p>
                                    </div>
                                </a>
                            </li>

                        @php
                            endwhile; wp_reset_query();
                        @endphp
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
