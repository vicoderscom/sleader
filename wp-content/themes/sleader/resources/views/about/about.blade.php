<div class="daotao">
    <div class="container submenu">
        @php
            $params = [
                'hierarchical'     => 1,
                'show_option_none' => '',
                'hide_empty'       => 0,
                'parent'           => 0,
                'taxonomy'         => 'categoris_about',
            ];

            $categories = get_categories($params);

            foreach ($categories as $value_order) {
                $order = get_field('order', 'categoris_about_' . $value_order->term_id);
                $value_order->order = $order;
            }

            usort($categories, function($a, $b) {
                return ($a->order <= $b->order);
            });

            foreach ($categories as $value) {
            // echo "<pre>";
            // var_dump($value);
            // $summary = get_field('content_summary', 'categoris_about_' . $value->term_id);

            $images = get_field('images', 'categoris_about_' . $value->term_id);

            $url = get_category_link($value->term_id);
        @endphp
                <div class="row">
                    <a href="{{ $url }}">
                        <div class="col-md-1"></div>
                        
                        <div class="col-md-10">
                            <div class="submenu_daotao">
                                <div class="col-md-2 col-sm-2">
                                    <img src="{!! $images !!}" alt="">
                                </div>
                                <div class="col-md-10 col-sm-10">
                                    <p class="title_sub">{{ $value->name }}</p>
                                    <p class="single_sub">{!! $value->description !!}</p>
                                    <p class="view_daotao">@php echo __('See details', 'widget_submenu'); @endphp</p>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-1"></div>
                    </a>
                </div>
        @php
                }
        @endphp
    </div>
</div>
