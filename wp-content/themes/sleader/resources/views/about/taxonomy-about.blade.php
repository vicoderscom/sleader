<div class="about">
    <div class="container">
        @php
        	$object_current = get_queried_object();


            $title_header = get_field('title_header', 'categoris_about_' . $object_current->term_id);

            $content_header = get_field('content_header', 'categoris_about_' . $object_current->term_id);
        @endphp
            <div class="header_about">
                <p class="title_page">{!! $title_header !!}</p>
                <p class="mota_page">{!! $content_header !!}</p>
            </div>
        @php

        	$post_cat = [
				'post_type'=>'about',
                'posts_per_page' => -1,
                'post_status'    => 'publish',
                'tax_query'     => array(
			        array(
			            'taxonomy'          => 'categoris_about',
			            'terms'             => array($object_current->term_id),
			            'field'             => 'term_id',
			            'operator'          => 'AND'
			        )
			    ),
			];

			$get_post_categoris = new WP_Query($post_cat);

			if($get_post_categoris->have_posts()){
	            foreach ($get_post_categoris->posts as $value) {
        @endphp

                <div class="sub_menu" href="#{{ $value->ID }}" data-toggle="collapse">
                    @php
                        if(ICL_LANGUAGE_CODE==en){
                            echo '<img class="img_read" src=" ' .get_stylesheet_directory_uri() . '/resources/assets/images/about/read_more.png" alt="">';
                        }else{
                            echo '<img class="img_read" src=" ' .get_stylesheet_directory_uri() . '/resources/assets/images/about/1.png" alt="">';
                        }


                        if(ICL_LANGUAGE_CODE==en){
                            echo '<img class="img_close" src=" ' .get_stylesheet_directory_uri() . '/resources/assets/images/about/close.png" alt="">';
                        }else{
                            echo '<img class="img_close" src=" ' .get_stylesheet_directory_uri() . '/resources/assets/images/about/2.png" alt="">';
                        }
                    @endphp

                    <p class="title_submu">
                        <a href="">{{ $value->post_title }}</a>
                    </p>

                    <p class="mota_submu">{{ $value->post_excerpt }}</p>

                    <div id="{{ $value->ID }}" class="collapse content_about">
                        {!! $value->post_content !!}
                    </div>
                </div>

        @php
                }
            }
        @endphp
    </div>
</div>
