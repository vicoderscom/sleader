<div class="col-md-9 filter_khoahoc">
	<ul class="list_khuvuc">
		@php

			while ($query->have_posts()) {
            $query->the_post();

			$img_book = wp_get_attachment_url(get_post_thumbnail_id($post->ID));

            $link_book = get_permalink($post->ID);

            $title_book = get_the_title($post->ID);

            $mota_book = get_the_excerpt($post->ID);

            $bat_dau = get_field('ngay_bat_dau', $post->ID);

            $ket_thuc = get_field('ngay_ket_thuc', $post->ID);

            $status = get_field('trang_thai', $post->ID);

            $location = wp_get_post_terms(get_the_ID(), 'categoty_location');

                
            if (!empty($location)) {
                
                $locations = [];

                foreach ($location as $key => $value) {
                    $locations[] = $value->name;
                }

                $locationStr = implode(', ', $locations);

            }

            $thoi_luong = get_field('thoi_luong', $post->ID)

            // var_dump($locationStr);
            // exit();
		@endphp

			<li class="row">
				<a href="{{ $link_book }}">
					<div>
			            <div class="col-md-4 col-sm-4 col-xs-12 image_list">
			            	<img style="background-image: url({{ $img_book }});" src="{{ get_stylesheet_directory_uri() }}/resources/assets/images/khoahoc/list_khoahoc.png" alt="">
			            </div>
			            <div class="col-md-8 col-sm-8 col-xs-12">
			            	<p class="date_khoahoc">@php echo __('Time:', 'daotao'); @endphp {{ $thoi_luong }}</p>

			            	<p class="title_kh">{{ $title_book }}</p>

			            	<p class="mota_kh">{{ $mota_book }}</p>

			            	<span class="status">{{ $status }}</span>
			            	<span class="read_more">@php echo __('read more', 'daotao_1'); @endphp <i class="fa fa-chevron-right" aria-hidden="true"></i></span>
			            </div>
					</div>
			    </a>
			</li>

		@php
        	}
    	@endphp
	</ul>
</div>

<div class="col-md-3 sidebar">
	<div class="sidebar_tintuc">
    	<div class="title">
    		<h4 class="aside-title">
	            @php
	            	echo __('News', 'sidebar-sleader');
	            @endphp
	            <a href="{{ site_url('sleader-truyen-thong') }}">@php echo __('See All News', 'sidebar-sleader'); @endphp</a>
	        </h4>

	        <div class="list_events">
	        	<ul>
					@php
						$events_sidebar = array(
							'post_type'      => 'news',
							'posts_per_page' => 5,
						);

						$sidebar = new WP_Query($events_sidebar);

						while ( $sidebar->have_posts() ) : $sidebar->the_post();

						$img_events =wp_get_attachment_url(get_post_thumbnail_id($post->ID));

						$link_events = get_permalink($post->ID);

                        $date = get_post_time( 'l, d-F-Y', false, $post->ID, 'vi' );

                        $single_mobile = get_the_excerpt();

					@endphp

		        		<li class="row">
		        			<a href="{{ $link_events }}">
			        			<div class="col-md-4 col-sm-4 col-xs-12 images_events">
			        				<img style="background-image: url({{ $img_events }});" src="{{ get_stylesheet_directory_uri() }}/resources/assets/images/sidebar/events.png" alt="">
			        			</div>

			        			<div class="col-md-8 col-sm-8 col-xs-12">
			        				<p class="title_events">
			        					@php
			        						the_title();
			        					@endphp
			        				</p>

			        				<p class="single_mobile">{{ $single_mobile }}</p>

			        				<p class="date_events">
			        					@php
			        						echo $date;
			        					@endphp
			        				</p>
			        			</div>
		        			</a>
		        		</li>

	        		@php
	        			endwhile; wp_reset_query();
	        		@endphp
	        	</ul>
	        </div>
    	</div>
    </div>
</div>