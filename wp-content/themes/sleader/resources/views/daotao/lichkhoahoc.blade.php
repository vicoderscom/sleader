<div class="container lichkhoahoc">
    <div class="row">
        <div class="col-md-9 filter_khoahoc">
            <form>
            	@php
	                echo do_shortcode('[filter]');
            	@endphp
            </form>

            @php
            	//var_dump($_GET['_display']);

            	$number_khoahoc = wp_count_posts( $post_type = 'khoahoc' );

            	if(empty($_GET['_display'])){
            		echo do_shortcode('[listing-khoahoc post_type="khoahoc" filter="yes" per_page="10" paged="yes"]');
            	}else{
            		echo do_shortcode('[listing-khoahoc post_type="khoahoc" filter="yes" per_page='. $_GET['_display'] .' paged="yes"]');
            	}

            @endphp
        </div>
        <div class="col-md-3 sidebar">
            <div class="sidebar_tintuc">
            	<div class="title">
            		<h4 class="aside-title">
			            @php
			            	echo __('News', 'sidebar-sleader');
			            @endphp
			            <a href="{{ site_url('sleader-truyen-thong') }}">@php echo __('See All News', 'sidebar-sleader'); @endphp</a>
			        </h4>

			        <div class="list_events">
			        	<ul>
							@php
								$events_sidebar = array(
									'post_type'      => 'news',
									'posts_per_page' => 5,
								);

								$sidebar = new WP_Query($events_sidebar);

								while ( $sidebar->have_posts() ) : $sidebar->the_post();

								$img_events =wp_get_attachment_url(get_post_thumbnail_id($post->ID));

								$link_events = get_permalink($post->ID);

                                $date = get_post_time( 'l, d-F-Y', false, $post->ID, 'vi' );

                                $single_mobile = get_the_excerpt();

							@endphp

				        		<li class="row">
				        			<a href="{{ $link_events }}">
					        			<div class="col-md-4 col-sm-4 col-xs-12 images_events">
					        				<img style="background-image: url({{ $img_events }});" src="{{ get_stylesheet_directory_uri() }}/resources/assets/images/sidebar/events.png" alt="">
					        			</div>

					        			<div class="col-md-8 col-sm-8 col-xs-12">
					        				<p class="title_events">
					        					@php
					        						the_title();
					        					@endphp
					        				</p>

					        				<p class="single_mobile">
					        					{{ $single_mobile }}
					        				</p>

					        				<p class="date_events">
					        					@php
					        						echo $date;
					        					@endphp
					        				</p>
					        			</div>
				        			</a>
				        		</li>

			        		@php
			        			endwhile; wp_reset_query();
			        		@endphp
			        	</ul>
			        </div>
            	</div>
            </div>
        </div>
    </div>
</div>
