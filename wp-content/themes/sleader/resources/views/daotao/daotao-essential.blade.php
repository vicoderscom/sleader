<div class="row pod-container col-count-4 slick-grid">
    @php
        while ($query->have_posts()) {
            $query->the_post();

            $thumbnail = wp_get_attachment_url(get_post_thumbnail_id());

            $title = get_the_title();

            $content = get_the_content();
    @endphp
        <div class="col-md-4 col-sm-4 item_daotao">
            <div class="colum">
                <div class="pod-image">
                    <img src="{{ $thumbnail }}" alt="Foundations of Directorship">
                </div>
                <div class="pod-content">
                    <h4 class="heading"><p>{{ $title }}</p></h4>
                    <div class="content">
                        <p>{{ $content }}</p>
                    </div>
                </div>
            </div>
        </div>
    @php
        }
    @endphp
</div>