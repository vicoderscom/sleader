<div class="date_i18n">
      <p>@php echo date_i18n('F Y'); @endphp</p>
</div>
<li class="row">
	<a href="{{ $url }}">
		<div>
            <div class="col-md-4 col-sm-4 image_list">
            	<img style="background-image: url({{ $thumbnail }});" src="{{ get_stylesheet_directory_uri() }}/resources/assets/images/khoahoc/list_khoahoc.png" alt="">
            </div>
            <div class="col-md-8 col-md-8">
                  <p class="date_khoahoc">@php echo __('Time:', 'daotao'); @endphp {{ $thoi_luong }}</p>

            	<p class="title_kh">{{ $title }}</p>

            	<p class="mota_kh">{{ $excerpt }}</p>

            	<span class="status">{{ $status }}</span>
            	<span class="read_more">@php echo __('read more', 'daotao'); @endphp <i class="fa fa-chevron-right" aria-hidden="true"></i></span>
            </div>
		</div>
    </a>
</li>