<div class="tuvan_nghiencuu">
	<div class="container">
		<div class="submenu_tuavn">
			<div class="row">

				@php
					if(ICL_LANGUAGE_CODE==en){
						dynamic_sidebar('widget_tuvan');
					}else{
						dynamic_sidebar('widget_tuvan_vi');
					}
				//dynamic_sidebar('widget_tuvan');

				@endphp

			</div>
		</div>
	</div>
	@php
		if(ICL_LANGUAGE_CODE==en){
			dynamic_sidebar('widget_video_tuvan');
		}else{
			dynamic_sidebar('widget_video_tuvan_vi');
		}
	//dynamic_sidebar('widget_video_tuvan');
	@endphp
</div>