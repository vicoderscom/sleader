<div class="tuvanchienluoc">
	<div class="container">
		<div class="item_chienluoc row">
			@php
				while ( have_posts() ) : the_post();

				$title_header = get_field( 'title_header', get_the_ID() );

				$content_header = get_field( 'content_header', get_the_ID() );

				$title_content = get_field( 'title_content', get_the_ID() );

				$content_page = get_field( 'content_body', get_the_ID() );

				$author = get_field( 'author', get_the_ID() );

				$view_all = get_field( 'view_all_page', get_the_ID() );

			@endphp
				<section id="meta-15" class="widget widget_meta">
					<h2 class="widget-title">{!! $title_header !!}</h2>
				</section>

				@foreach ($content_header as $value)

					<div class="col-md-4 item_supper">
		                <div class="item_tuvan">
		                    <div class="images">
		                        <img src="{{ $value["images"]['url'] }}" alt="">
		                    </div>

		                    <div class="content_item">
		                        <p class="title_item">{{ $value["title"] }}</p>

		                        <p class="single_item">
		                            {!! $value["content"] !!}
		                        </p>
		                    </div>
		                </div>
	            	</div>

            	@endforeach
		</div>
	</div>

	@php
		echo $title_content;
	@endphp
		@foreach ($content_page as $value_page)
			<div class="learn">
	            <div class="container">
	                <div class="noidung">
	                    <ul>
	                        <li class="row">
	                            <div class="col-md-2 col-sm-3 images_learn">
	                                <img src="{{ $value_page["images"]['url'] }}" alt="Foundations of Directorship">
	                            </div>
	                            <div class="col-md-10 col-sm-9 single_learn">
	                                <p class="title_post">{{ $value_page["title"] }}</p>

	                                <div class="single_post">
	                                	<p>{!! $value_page["content_text"] !!}</p>
	                                </div>
	                            </div>
	                        </li>
	                    </ul>
	                </div>
	            </div>
	        </div>
        @endforeach;

        @if($author)
	        <div class="author container">
	            <div class="row">
	                <div class="col-sm-6 col-md-4 col-md-offset-1 images_author">
	                    <img style="background-image:url({{ $author[0]['images']['url'] }});" src="@php echo get_stylesheet_directory_uri() @endphp/resources/assets/images/tuvan/tacgia.png" alt="">
	                </div>
	                @php
	                	echo $author[0]['content'];
	                @endphp
	            </div>
	        </div>
        @endif
	@php
		if (!empty($view_all)) {
			echo $view_all;
		}

		endwhile;
    	wp_reset_query();
	@endphp

</div>