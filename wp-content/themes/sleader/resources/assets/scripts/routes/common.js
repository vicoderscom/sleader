import meanmenu from 'jquery.meanmenu/jquery.meanmenu.js';
import slick from "slick-carousel/slick/slick.js";
// using this import type for require third 3rd. For example:
// import Wow from 'wow.js';
 
// or you can specify the script file which you want to import
// import meanmenu from 'jquery.meanmenu/jquery.meanmenu.js';

import URI from 'urijs';

export default {
  init() {
  	$('#myNavbar').meanmenu({
        meanScreenWidth: "992",
        meanMenuContainer: ".mobile-menu",
    });

  	var count = 0;
    $('.sub_menu').click(function(event) {
    	var readBtn = $(this).find('.img_read');

    	var closeBtn = $(this).find('.img_close');
    	if (count %2 === 0) {
    		readBtn.css({'display':'none'});
    		closeBtn.css({'display':'block'});
    	}else{
    		readBtn.css({'display':'block'});
    		closeBtn.css({'display':'none'});
    	}
    	count++;
    });

    //phan trang dao tao khu vuc va doanh nghiep
    
    $('.pagination').insertAfter('.list_khuvuc');

    // ==============================================
  	
    // JavaScript to be fired on all pages

    $(document).ready(function(){
		var filterSelect = $('.go_filter');

	    filterSelect.click(function(event){

	    	event.preventDefault();

	    	var uri = new URI();
	   		
			$('.filter-control').each(function() {

				var value = $(this).find('select').val();
		        var type = $(this).find('select').attr('data-type');
		        var key = $(this).find('select').attr('data-key');
		    	
		    	//console.log(key);

		        switch (type) {
		            case 'tax':
		                if (uri.hasQuery(key)) {
		                    uri.setQuery(key, value);
		                } else {
		                    uri.addQuery(key, value);
		                }
		                break;

		            default:
		                if (uri.hasQuery(key)) {
		                    uri.setQuery(key, value);
		                } else {
		                    uri.addQuery(key, value);
		                }
		                break;
		        }

		        if (uri.segment(1) == 'page') {
		            uri.segment(2, "1");
		        }
		       
			});


			//console.log(uri);

			window.location = uri.href();

	    });
	});

	// search trang chu

	$(document).ready(function(){
	  // Add smooth scrolling to all links
	  
	  $(".search-head").on('click', function(event) {
	      $('html, body').animate({
	        scrollTop: $('#search_home').offset().top
	      }, 800, function(){

	      });
	  });

	  //js tu van

	  $(".item_supper").on('click', function(event) {
	  	var index = $(this).index();
	  	//console.log('index ', index);
	      $('html, body').animate({
	        scrollTop: $('.learn .noidung').eq(index - 1).offset().top
	      }, 800, function(){

	      });
	  });

	  $('#user_email').keyup(function(event) {
	   		var email_val = $(this).val();
			
			$('.user_register').val(email_val);
		})

	});

	//them class images-responsive cho single tin tuc su kien
	$( ".detail_news .content_single img" ).addClass( "img-responsive" );

	// ===================================

	$( "#page_id" ).click(function() {
		$( "#search_page_item" ).toggle("slow");
	});

	$('#work').insertAfter('#full_name');

	// =========================================

	// var $iframe = $('iframe'),
 //    src = $iframe.data('src');

	// if (window.matchMedia("(min-width: 480px)").matches) {
	//     $iframe.attr('src', src);
	// }

	// ======================================
	// console.log(site_url);
	$(document).ready(function(){
		$('#apipost_highlight_next').on('click',function(event){
	    	event.preventDefault();
			var number_page = $(this).attr('currpage');
			number_page = parseInt(number_page);
			if(number_page <= 1 ) {
				number_page = 1;
			}
	    	$.ajax({
	    		url: site_url + "/wp-json/wp/v2/highlight-post",
	    		type: "POST",
	    		data: {
	    			number_page: (number_page + 1)
	    		},
	    		beforeSend: function() {
	    			$('.list_khoahoc.right .fa-spinner').removeClass('hide');
	    			$('.list_khoahoc.right a').addClass('hide');
	    		},
	    		complete: function(){
	    			$('.list_khoahoc.right a').removeClass('hide');
	    			$('.list_khoahoc.right .fa-spinner').addClass('hide');
	    		},
	    		success: function(response){
	    			// console.log(response);
	    			var res = $.parseJSON(JSON.stringify(response));
	    			if((res.max_num_pages >= number_page) && (res.res_html !== '') && (number_page >= 1)) {
	    				$('#apipost_highlight').attr('currpage', (parseInt(number_page) + 1));
	    				$('.show_ajaxpost').html(res.res_html);
	    				$('.hide_ajaxshow').hide();
	    			} 
	    		}
	    	});
	    });
	    $('#apipost_highlight_prev').on('click',function(event){
	    	event.preventDefault();
			var number_page = $(this).attr('currpage');
			number_page = parseInt(number_page);
			if(number_page <= 1 ) {
				number_page = 2;
			}
	    	$.ajax({
	    		url: site_url + "/wp-json/wp/v2/highlight-post",
	    		type: "POST",
	    		data: {
	    			number_page: number_page - 1
	    		},
	    		beforeSend: function() {
	    			$('.list_khoahoc.left .fa-spinner').removeClass('hide');
	    			$('.list_khoahoc.left a').addClass('hide');
	    		},
	    		complete: function(){
	    			$('.list_khoahoc.left a').removeClass('hide');
	    			$('.list_khoahoc.left .fa-spinner').addClass('hide');
	    		},
	    		success: function(response){
	    			// console.log(response);
	    			var res = $.parseJSON(JSON.stringify(response));
	    			if((res.max_num_pages >= number_page) && (res.res_html !== '') && (number_page >= 1)) {
	    				$('#apipost_highlight').attr('currpage', (number_page - 1));
	    				$('.show_ajaxpost').html(res.res_html);
	    				$('.hide_ajaxshow').hide();
	    			} 
	    		}
	    	});
	    });
	});

//Khóa học homepage 13/12/2017
	$(document).ready(function(){
		$('#apipost_courses_next').on('click',function(event){
	    	event.preventDefault();
			var number_page = $(this).attr('currpage');
			number_page = parseInt(number_page);
			if(number_page <= 1 ) {
				number_page = 1;
			}
	    	$.ajax({
	    		url: site_url + "/wp-json/wp/v2/scienve-post",
	    		type: "POST",
	    		data: {
	    			number_page: (number_page + 1)
	    		},
	    		beforeSend: function() {
	    			$('.list_khoahoc.right_courses .fa-spinner').removeClass('hide');
	    			$('.list_khoahoc.right_courses a').addClass('hide');
	    		},
	    		complete: function(){
	    			$('.list_khoahoc.right_courses a').removeClass('hide');
	    			$('.list_khoahoc.right_courses .fa-spinner').addClass('hide');
	    		},
	    		success: function(response){
	    			// console.log(response);
	    			var res = $.parseJSON(JSON.stringify(response));
	    			if((res.max_num_pages >= number_page) && (res.res_html !== '') && (number_page >= 1)) {
	    				$('#apipost_highlight').attr('currpage', (parseInt(number_page) + 1));
	    				$('.show_ajaxpost_courses').html(res.res_html);
	    				$('.hide_ajaxshow_courses').hide();
	    			} 
	    		}
	    	});
	    });
	    $('#apipost_courses_prev').on('click',function(event){
	    	event.preventDefault();
			var number_page = $(this).attr('currpage');
			number_page = parseInt(number_page);
			if(number_page <= 1 ) {
				number_page = 2;
			}
	    	$.ajax({
	    		url: site_url + "/wp-json/wp/v2/scienve-post",
	    		type: "POST",
	    		data: {
	    			number_page: number_page - 1
	    		},
	    		beforeSend: function() {
	    			$('.list_khoahoc.left_courses .fa-spinner').removeClass('hide');
	    			$('.list_khoahoc.left_courses a').addClass('hide');
	    		},
	    		complete: function(){
	    			$('.list_khoahoc.left_courses a').removeClass('hide');
	    			$('.list_khoahoc.left_courses .fa-spinner').addClass('hide');
	    		},
	    		success: function(response){
	    			// console.log(response);
	    			var res = $.parseJSON(JSON.stringify(response));
	    			if((res.max_num_pages >= number_page) && (res.res_html !== '') && (number_page >= 1)) {
	    				$('#apipost_highlight').attr('currpage', (number_page - 1));
	    				$('.show_ajaxpost_courses').html(res.res_html);
	    				$('.hide_ajaxshow_courses').hide();
	    			} 
	    		}
	    	});
	    });
	});

//chia sẻ chi thức 13/12/2017

	$(document).ready(function(){
		$('#apipost_share_next').on('click',function(event){
	    	event.preventDefault();
			var number_page = $(this).attr('currpage');
			number_page = parseInt(number_page);
			if(number_page <= 1 ) {
				number_page = 1;
			}
	    	$.ajax({
	    		url: site_url + "/wp-json/wp/v2/cat-post",
	    		type: "POST",
	    		data: {
	    			number_page: (number_page + 1)
	    		},
	    		beforeSend: function() {
	    			$('.list_khoahoc.right_share .fa-spinner').removeClass('hide');
	    			$('.list_khoahoc.right_share a').addClass('hide');
	    		},
	    		complete: function(){
	    			$('.list_khoahoc.right_share a').removeClass('hide');
	    			$('.list_khoahoc.right_share .fa-spinner').addClass('hide');
	    		},
	    		success: function(response){
	    			// console.log(response);
	    			var res = $.parseJSON(JSON.stringify(response));
	    			if((res.max_num_pages >= number_page) && (res.res_html !== '') && (number_page >= 1)) {
	    				$('#apipost_highlight').attr('currpage', (parseInt(number_page) + 1));
	    				$('.show_ajaxpost_share').html(res.res_html);
	    				$('.hide_ajaxshow_share').hide();
	    			} 
	    		}
	    	});
	    });
	    $('#apipost_share_prev').on('click',function(event){
	    	event.preventDefault();
			var number_page = $(this).attr('currpage');
			number_page = parseInt(number_page);
			if(number_page <= 1 ) {
				number_page = 2;
			}
	    	$.ajax({
	    		url: site_url + "/wp-json/wp/v2/cat-post",
	    		type: "POST",
	    		data: {
	    			number_page: number_page - 1
	    		},
	    		beforeSend: function() {
	    			$('.list_khoahoc.left_share .fa-spinner').removeClass('hide');
	    			$('.list_khoahoc.left_share a').addClass('hide');
	    		},
	    		complete: function(){
	    			$('.list_khoahoc.left_share a').removeClass('hide');
	    			$('.list_khoahoc.left_share .fa-spinner').addClass('hide');
	    		},
	    		success: function(response){
	    			// console.log(response);
	    			var res = $.parseJSON(JSON.stringify(response));
	    			if((res.max_num_pages >= number_page) && (res.res_html !== '') && (number_page >= 1)) {
	    				$('#apipost_highlight').attr('currpage', (number_page - 1));
	    				$('.show_ajaxpost_share').html(res.res_html);
	    				$('.hide_ajaxshow_share').hide();
	    			} 
	    		}
	    	});
	    });
	});


// =====================================================================

	$(document).ready(function(){
		$('#apipost_highlight_next_kh').on('click',function(event){
	    	event.preventDefault();
			var number_page = $(this).attr('currpage');
			number_page = parseInt(number_page);
			if(number_page <= 1 ) {
				number_page = 1;
			}
	    	$.ajax({
	    		url: site_url + "/wp-json/wp/v2/gocnhin-post",
	    		type: "POST",
	    		data: {
	    			number_page: (number_page + 1)
	    		},
	    		beforeSend: function() {
	    			$('.list_khoahoc.right_kh .fa-spinner').removeClass('hide');
	    			$('.list_khoahoc.right_kh a').addClass('hide');
	    		},
	    		complete: function(){
	    			$('.list_khoahoc.right_kh a').removeClass('hide');
	    			$('.list_khoahoc.right_kh .fa-spinner').addClass('hide');
	    		},
	    		success: function(response){
	    			// console.log(response);
	    			var res = $.parseJSON(JSON.stringify(response));
	    			if((res.max_num_pages >= number_page) && (res.res_html !== '') && (number_page >= 1)) {
	    				$('#apipost_highlight').attr('currpage', (parseInt(number_page) + 1));
	    				$('.show_ajaxpost_kh').html(res.res_html);
	    				$('.hide_ajaxshow_khoahoc').hide();
	    			} 
	    		}
	    	});
	    });
	    $('#apipost_highlight_prev_kh').on('click',function(event){
	    	event.preventDefault();
			var number_page = $(this).attr('currpage');
			number_page = parseInt(number_page);
			if(number_page <= 1 ) {
				number_page = 2;
			}
	    	$.ajax({
	    		url: site_url + "/wp-json/wp/v2/gocnhin-post",
	    		type: "POST",
	    		data: {
	    			number_page: number_page - 1
	    		},
	    		beforeSend: function() {
	    			$('.list_khoahoc.left_kh .fa-spinner').removeClass('hide');
	    			$('.list_khoahoc.left_kh a').addClass('hide');
	    		},
	    		complete: function(){
	    			$('.list_khoahoc.left_kh a').removeClass('hide');
	    			$('.list_khoahoc.left_kh .fa-spinner').addClass('hide');
	    		},
	    		success: function(response){
	    			// console.log(response);
	    			var res = $.parseJSON(JSON.stringify(response));
	    			if((res.max_num_pages >= number_page) && (res.res_html !== '') && (number_page >= 1)) {
	    				$('#apipost_highlight').attr('currpage', (number_page - 1));
	    				$('.show_ajaxpost_kh').html(res.res_html);
	    				$('.hide_ajaxshow_khoahoc').hide();
	    			} 
	    		}
	    	});
	    });
	});

	// $('.slick_post').slick({
	//   slidesToShow: 1,
	//   slidesToScroll: 1,
	//   prevArrow: '<span class="a-left control-c prev slick-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></span>',
 //      nextArrow: '<span class="a-right control-c next slick-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></span>',
	// });
  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};
