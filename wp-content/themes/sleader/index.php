<?php
use NF\View\Facades\View;

get_header();

echo View::render('home.home');

get_footer();