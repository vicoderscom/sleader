<?php

//echo 'ok';exit;

$app = require_once __DIR__ . '/bootstrap/app.php';

include(TEMPLATEPATH . '/app/Widgets/WidgetDaoTao.php');

add_action("after_download", "download_notification",10,1); 
function download_notification($package){ 
    $pak = get_post($package['ID']);
    var_dump($pak); die;
}

// custom logo 
class setting {
	function __construct() {
		add_theme_support( 'custom-logo' );
	}
}
new setting();


if(!function_exists('setup_site')) {
	function setup_site() {
		register_nav_menus( array(
            'primary' => __( 'Menu chính', 'sleader' ),
        ) );

        // add_theme_support('menus');
        add_theme_support('post-thumbnails');
	}
	add_action('init', 'setup_site');
}


if (!function_exists('widget_register')) {
	function widget_register() {
		

        if(ICL_LANGUAGE_CODE==en){
            $sidebars = [
                [
                    'name'          => __('Page Training', 'sleader'),
                    'id'            => 'dao_tao',
                    'description'   => __('This is sidebar for header text', 'sleader'),
                    'before_widget' => '<section id="%1$s" class="widget %2$s">',
                    'after_widget'  => '</section>',
                    'before_title'  => '<h2 class="widget-title">',
                    'after_title'   => '</h2>',
                ],

                [
                    'name'          => __('Content Page Training', 'sleader'),
                    'id'            => 'categoris_daotao',
                    'description'   => __('This is sidebar for header text', 'sleader'),
                    'before_widget' => '<section id="%1$s" class="widget %2$s">',
                    'after_widget'  => '</section>',
                    'before_title'  => '<h2 class="widget-title">',
                    'after_title'   => '</h2>',
                ],

                [
                    'name'          => __('Page Event', 'sleader'),
                    'id'            => 'template_sukien',
                    'description'   => __('This is sidebar for header text', 'sleader'),
                    'before_widget' => '<section id="%1$s" class="widget %2$s">',
                    'after_widget'  => '</section>',
                    'before_title'  => '<h2 class="widget-title">',
                    'after_title'   => '</h2>',
                ],

                [
                    'name'          => __('Consulting and Research', 'sleader'),
                    'id'            => 'widget_tuvan',
                    'description'   => __('This is sidebar for header text', 'sleader'),
                    'before_widget' => '<section id="%1$s" class="widget %2$s">',
                    'after_widget'  => '</section>',
                    'before_title'  => '<h2 class="widget-title">',
                    'after_title'   => '</h2>',
                ],

                [
                    'name'          => __('Video page Consulting and Research', 'sleader'),
                    'id'            => 'widget_video_tuvan',
                    'description'   => __('This is sidebar for header text', 'sleader'),
                    'before_widget' => '<section id="%1$s" class="widget %2$s">',
                    'after_widget'  => '</section>',
                    'before_title'  => '<h2 class="widget-title">',
                    'after_title'   => '</h2>',
                ],

                [
                    'name'          => __('Page Document', 'sleader'),
                    'id'            => 'widget_tai_lieu',
                    'description'   => __('This is sidebar for header text', 'sleader'),
                    'before_widget' => '<section id="%1$s" class="widget %2$s">',
                    'after_widget'  => '</section>',
                    'before_title'  => '<h2 class="widget-title">',
                    'after_title'   => '</h2>',
                ],

                [
                    'name'          => __('Page Contact', 'sleader'),
                    'id'            => 'widget_lienhe',
                    'description'   => __('This is sidebar for header text', 'sleader'),
                    'before_widget' => '<section id="%1$s" class="widget %2$s">',
                    'after_widget'  => '</section>',
                    'before_title'  => '<h2 class="widget-title">',
                    'after_title'   => '</h2>',
                ],

                [
                    'name'          => __('Footer', 'sleader'),
                    'id'            => 'widget_footer',
                    'description'   => __('This is sidebar for header text', 'sleader'),
                    'before_widget' => '<section id="%1$s" class="widget %2$s">',
                    'after_widget'  => '</section>',
                    'before_title'  => '<h2 class="widget-title">',
                    'after_title'   => '</h2>',
                ],

                [
                    'name'          => __('Icon MXH Header', 'sleader'),
                    'id'            => 'widget_icon_header',
                    'description'   => __('This is sidebar for header text', 'sleader'),
                    'before_widget' => '<section id="%1$s" class="widget %2$s">',
                    'after_widget'  => '</section>',
                    'before_title'  => '<h2 class="widget-title">',
                    'after_title'   => '</h2>',
                ],

                [
                    'name'          => __('Logo', 'sleader'),
                    'id'            => 'widget_logo',
                    'description'   => __('This is sidebar for header text', 'sleader'),
                    'before_widget' => '<section id="%1$s" class="widget %2$s">',
                    'after_widget'  => '</section>',
                    'before_title'  => '<h2 class="widget-title">',
                    'after_title'   => '</h2>',
                ],

                [
                    'name'          => __('Hotline', 'sleader'),
                    'id'            => 'widget_hotline',
                    'description'   => __('This is sidebar for header text', 'sleader'),
                    'before_widget' => '<section id="%1$s" class="widget %2$s">',
                    'after_widget'  => '</section>',
                    'before_title'  => '<h2 class="widget-title">',
                    'after_title'   => '</h2>',
                ],

                [
                    'name'          => __('Silder', 'sleader'),
                    'id'            => 'widget_silder',
                    'description'   => __('This is sidebar for header text', 'sleader'),
                    'before_widget' => '<section id="%1$s" class="widget %2$s">',
                    'after_widget'  => '</section>',
                    'before_title'  => '<h2 class="widget-title">',
                    'after_title'   => '</h2>',
                ],

            ];
        }else{
            $sidebars = [
                [
                    'name'          => __('Trang Đào Tạo', 'sleader'),
                    'id'            => 'dao_tao_vi',
                    'description'   => __('This is sidebar for header text', 'sleader'),
                    'before_widget' => '<section id="%1$s" class="widget %2$s">',
                    'after_widget'  => '</section>',
                    'before_title'  => '<h2 class="widget-title">',
                    'after_title'   => '</h2>',
                ],

                [
                    'name'          => __('Nội Dung Trang Đào Tạo', 'sleader'),
                    'id'            => 'categoris_daotao_vi',
                    'description'   => __('This is sidebar for header text', 'sleader'),
                    'before_widget' => '<section id="%1$s" class="widget %2$s">',
                    'after_widget'  => '</section>',
                    'before_title'  => '<h2 class="widget-title">',
                    'after_title'   => '</h2>',
                ],

                [
                    'name'          => __('Trang Sự Kiện', 'sleader'),
                    'id'            => 'template_sukien_vi',
                    'description'   => __('This is sidebar for header text', 'sleader'),
                    'before_widget' => '<section id="%1$s" class="widget %2$s">',
                    'after_widget'  => '</section>',
                    'before_title'  => '<h2 class="widget-title">',
                    'after_title'   => '</h2>',
                ],

                [
                    'name'          => __('Tư Vấn Và Nghiên Cứu', 'sleader'),
                    'id'            => 'widget_tuvan_vi',
                    'description'   => __('This is sidebar for header text', 'sleader'),
                    'before_widget' => '<section id="%1$s" class="widget %2$s">',
                    'after_widget'  => '</section>',
                    'before_title'  => '<h2 class="widget-title">',
                    'after_title'   => '</h2>',
                ],

                [
                    'name'          => __('Video page Tư Vấn Nghiên Cứu', 'sleader'),
                    'id'            => 'widget_video_tuvan_vi',
                    'description'   => __('This is sidebar for header text', 'sleader'),
                    'before_widget' => '<section id="%1$s" class="widget %2$s">',
                    'after_widget'  => '</section>',
                    'before_title'  => '<h2 class="widget-title">',
                    'after_title'   => '</h2>',
                ],

                [
                    'name'          => __('Trang Tài Liệu', 'sleader'),
                    'id'            => 'widget_tai_lieu_vi',
                    'description'   => __('This is sidebar for header text', 'sleader'),
                    'before_widget' => '<section id="%1$s" class="widget %2$s">',
                    'after_widget'  => '</section>',
                    'before_title'  => '<h2 class="widget-title">',
                    'after_title'   => '</h2>',
                ],

                [
                    'name'          => __('Trang Liên Hệ', 'sleader'),
                    'id'            => 'widget_lienhe_vi',
                    'description'   => __('This is sidebar for header text', 'sleader'),
                    'before_widget' => '<section id="%1$s" class="widget %2$s">',
                    'after_widget'  => '</section>',
                    'before_title'  => '<h2 class="widget-title">',
                    'after_title'   => '</h2>',
                ],

                [
                    'name'          => __('Footer', 'sleader'),
                    'id'            => 'widget_footer_vi',
                    'description'   => __('This is sidebar for header text', 'sleader'),
                    'before_widget' => '<section id="%1$s" class="widget %2$s">',
                    'after_widget'  => '</section>',
                    'before_title'  => '<h2 class="widget-title">',
                    'after_title'   => '</h2>',
                ],

                [
                    'name'          => __('Icon MXH Header', 'sleader'),
                    'id'            => 'widget_icon_header_vi',
                    'description'   => __('This is sidebar for header text', 'sleader'),
                    'before_widget' => '<section id="%1$s" class="widget %2$s">',
                    'after_widget'  => '</section>',
                    'before_title'  => '<h2 class="widget-title">',
                    'after_title'   => '</h2>',
                ],

                [
                    'name'          => __('Logo', 'sleader'),
                    'id'            => 'widget_logo_vi',
                    'description'   => __('This is sidebar for header text', 'sleader'),
                    'before_widget' => '<section id="%1$s" class="widget %2$s">',
                    'after_widget'  => '</section>',
                    'before_title'  => '<h2 class="widget-title">',
                    'after_title'   => '</h2>',
                ],


                [
                    'name'          => __('Hotline', 'sleader'),
                    'id'            => 'widget_hotline_vi',
                    'description'   => __('This is sidebar for header text', 'sleader'),
                    'before_widget' => '<section id="%1$s" class="widget %2$s">',
                    'after_widget'  => '</section>',
                    'before_title'  => '<h2 class="widget-title">',
                    'after_title'   => '</h2>',
                ],

                [
                    'name'          => __('Silder', 'sleader'),
                    'id'            => 'widget_silder_vi',
                    'description'   => __('This is sidebar for header text', 'sleader'),
                    'before_widget' => '<section id="%1$s" class="widget %2$s">',
                    'after_widget'  => '</section>',
                    'before_title'  => '<h2 class="widget-title">',
                    'after_title'   => '</h2>',
                ],

            ];
        }

		foreach ($sidebars as $sidebar) {
			register_sidebar($sidebar);
		}
	}
	add_action('widgets_init', 'widget_register');
}


function custom_excerpt_length( $length ) {
    return 20;
        }
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

//get sunbmenu page
//
function get_nav_menu_item_children( $parent_id, $nav_menu_items, $depth = true ) {
    $nav_menu_item_list = array();
    foreach ( (array) $nav_menu_items as $nav_menu_item ) {
        if ( $nav_menu_item->menu_item_parent == $parent_id ) {
        $nav_menu_item_list[] = $nav_menu_item;
        if ( $depth ) {
            if ( $children = get_nav_menu_item_children( $nav_menu_item->ID, $nav_menu_items ) )
            $nav_menu_item_list = array_merge( $nav_menu_item_list, $children );
            }
        }
    }
    return $nav_menu_item_list;
}

add_action('get_nav_item_child', 'get_nav_menu_item_children', 10, 3);



//callback submenu

add_action('get_submenu', 'submenu_nav_item');

function submenu_nav_item($id = ''){
    $menuLocations = get_nav_menu_locations();
    $menuID = $menuLocations['primary'];
    $nav_menu_items = wp_get_nav_menu_items($menuID);

    if(empty($id)) {
        $this_item = current( wp_filter_object_list( $nav_menu_items, array( 'object_id' => get_queried_object_id() ) ) );
    } else {
        $this_item = current( wp_filter_object_list( $nav_menu_items, array( 'menu_item_parent' => $id ) ) );
    }


    // echo "<pre>";
    // var_dump($this_item); die;
    $parent_id = $this_item->ID;
    if($this_item->menu_item_parent != 0) {
        $parent_id = $this_item->menu_item_parent;
    }

    // echo "<pre>";
    // var_dump($nav_menu_items);
    // die;

    $depth = true;

    $nav_menu_item_list = array();
        foreach ( (array) $nav_menu_items as $nav_menu_item ) {
            if ( $nav_menu_item->menu_item_parent == $parent_id ) {
                $nav_menu_item_list[] = $nav_menu_item;

                if ( $depth ) {
                     if ( $children = get_nav_menu_item_children( $nav_menu_item->ID, $nav_menu_items ) )
                     $nav_menu_item_list = array_merge( $nav_menu_item_list, $children );
                }
            }
        }

        if(!empty($nav_menu_item_list)) {

        echo "<div class='submenu_item'>";
        foreach ($nav_menu_item_list as $key => $item) {
            $mang = explode("/",$item->url);

            // var_dump($mang);
            // exit();

            $new_array_about = [
                $mang[count($mang) - 2], 
                $mang[count($mang) - 3],
                $mang[count($mang) - 4]
            ];

            // var_dump($new_array_about);
            // exit();

            $new_array = [
                $mang[count($mang) - 3], 
                $mang[count($mang) - 2]
            ];

            $lastItem = $mang[(array_keys($mang)[(count($mang)-2)])];

            $lastItem = '/' . $lastItem . '/' ;

            $new_array = '/' . $new_array[0] . '/' . $new_array[1] . '/' ;

            $new_array_about = '/' . $new_array_about[2] . '/'. $new_array_about[1] . '/' . $new_array_about[0] . '/';

            // var_dump($new_array_about);
            // exit();

            if ($lastItem == $_SERVER['REQUEST_URI'] || $new_array == $_SERVER['REQUEST_URI'] || $new_array_about == $_SERVER['REQUEST_URI']) {
                $link_submenu = '<a href="'. $item->url .'" class="submenu_action">'. $item->title .'</a>';

                echo'<span style="list-style: none;" class="menu_children">';
                        echo $link_submenu;
                echo'</span>';
            }else{
                $link_submenu = '<a href="'. $item->url .'">'. $item->title .'</a>';

                echo'<span style="list-style: none;" class="menu_children">';
                        echo $link_submenu;
                echo'</span>';
            }

        }
        echo "</div>";
        }
}

//================================

/**
 * Add HTML5 theme support.
 */
function wpdocs_after_setup_theme() {
    add_theme_support( 'html5', array( 'search-form' ) );
}
add_action( 'after_setup_theme', 'wpdocs_after_setup_theme' );


//===========================================
//viet hoa phan trang

if(ICL_LANGUAGE_CODE==vi){
    function viethoa_pagination(){
        $Capitalization = [
            'tag'       => 'div',
            'class'     => 'pagination',
            'id'        => '',
            'prev_text' => '« Trang Trước',
            'next_text' => 'Trang Sau »'
        ];

        return $Capitalization;
    }

    add_filter( 'paged_wrap', 'viethoa_pagination' );
}

// var_dump(get_template_directory_uri());

function my_enqueue() {
    wp_enqueue_script( 'admin', get_template_directory_uri() . '/resources/assets/scripts/routes/admin.js' );
}
add_action( 'admin_enqueue_scripts', 'my_enqueue' );



//thêm phone data user
function new_contact_methods($contactmethods)
{
    $contactmethods['agency_work'] = 'Cơ quan công tác';
    return $contactmethods;
}
add_filter('user_contactmethods', 'new_contact_methods', 10, 1);

function new_modify_user_table($column)
{
    $column['agency_work'] = 'agency_work';
    return $column;
}
add_filter('manage_users_columns', 'new_modify_user_table');

function new_modify_user_table_row($val, $column_name, $user_id)
{

    switch ($column_name) {
        case 'agency_work':
            return get_the_author_meta('agency_work', $user_id);
            break;
        default:
    }
    return $val;
}
add_filter( 'manage_users_custom_column', 'new_modify_user_table_row', 10, 3 );

