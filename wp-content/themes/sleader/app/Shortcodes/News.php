<?php 
namespace App\Shortcodes;
use NF\Abstracts\ShortCode;
use MSC\Listing;
use MSC\View;


class News extends Listing
{
	
	public $shortcodename = 'news-sukien';

	public function __construct()
    {
        add_filter('wrap_tag', [$this, 'customTags']);
        add_filter('wrap_atts', [$this, 'customAtts']);

        parent::__construct();
    }

    public function customTags()
    {
        return 'ul';
    }

    public function customAtts()
    {
        return [
            'class' => 'list-khoahoc'
        ];
    }

    public function handle($query, $opts)
    {
        if ($query->have_posts()) {
            while ($query->have_posts()) {
                $query->the_post();
                $view = new View;
                // $view->setViewPath(__DIR__  . '/resources/views/');
                // $view->setCachePath(__DIR__  . '/storage/cache/');
                //var_dump($view->getViewPath());exit;


                $data = [
                    'id' => get_the_ID(),
                    'url' => get_permalink(),
                    'thumbnail' => wp_get_attachment_url(get_post_thumbnail_id()),
                    'title' => get_the_title(),
                    'date' => get_post_time( 'l, d-F-Y', false, $post->ID, 'vi' ),
                ];

                 echo $view->render('news.news-item', $data);
            }

        }
    }
}
 
?>