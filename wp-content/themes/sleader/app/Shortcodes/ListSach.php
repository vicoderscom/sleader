<?php
namespace App\Shortcodes;
use NF\Abstracts\ShortCode;
use MSC\Listing;
use MSC\View;

	class ListSach extends Listing
	{
		public $shortcodename = 'sach-anpham';

		public function __construct()
	    {	

	    	$this->setCustomAttrs([
	    		'tax_id' => null
	    	]);

	        parent::__construct();
	    }

		public function handle($query, $opts)
		{
			//var_dump($opts['tax_id']);
			if ($query->have_posts()) {

             //    echo "<pre>";
            	// var_dump($opts);
            	// exit();

				if ($opts['tax_id'] !== null) {
					$view = new View;
					$taxs = get_term($opts['tax_id']);

					$tile_taxs = $taxs->name;

					$description_tax = $taxs->description;

					$data = [
						'name_cat' => $tile_taxs,
						'description' => $description_tax,
					];

					echo $view->render('tailieu.sach_namecate', $data);
				}


				$view = new View;

				$data_wihle = [
					'query' => $query,
				];

	   			echo $view->render('tailieu.sach_list', $data_wihle);

        	}
		}
	}
?>