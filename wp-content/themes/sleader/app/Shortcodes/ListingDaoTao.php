<?php
namespace App\Shortcodes;
use NF\Abstracts\ShortCode;
use MSC\Listing;
use MSC\View;

	class ListingDaoTao extends Listing
	{
		public $shortcodename = 'essential-courses';

		public function __construct()
	    {	

	    	$this->setCustomAttrs([
	    		'tax_id' => null
	    	]);

	        parent::__construct();
	    }

		public function handle($query, $opts)
		{
			//var_dump($opts['tax_id']);
			if ($query->have_posts()) {

             //    echo "<pre>";
            	// var_dump($opts);

				if ($opts['tax_id'] !== null) {
					$view = new View;
					$taxs = get_term($opts['tax_id']);

					$tile_taxs = $taxs->name;

					$description_tax = $taxs->description;

					$data = [
						'name_cat' => $tile_taxs,
						'description' => $description_tax,
					];

					echo $view->render('daotao.essential-courses', $data);
				}


				$view = new View;

				$data_wihle = [
					'query' => $query,
				];
	   			echo $view->render('daotao.daotao-essential', $data_wihle);
	   			
	            // while ($query->have_posts()) {
	            //     $query->the_post();
	            //     $view = new View;

	            	
	            //     // $view->setViewPath(__DIR__  . '/resources/views/');
	            //     // $view->setCachePath(__DIR__  . '/storage/cache/');
	            //     //var_dump($view->getViewPath());exit;
	            //     $data_wihle = [
	            //         'title' => get_the_title(),
	            //         'thumbnail' => wp_get_attachment_url(get_post_thumbnail_id()),
	            //     ];

	            //     echo $view->render('daotao.daotao-essential', $data_wihle);
	            // }

        	}
		}
	}
?>