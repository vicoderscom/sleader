<?php 
namespace App\Shortcodes;
use NF\Abstracts\ShortCode;
use MSC\Listing;
use MSC\View;


class LichCacKhoaHoc extends Listing
{
	
	public $shortcodename = 'listing-khoahoc';

	public function __construct()
    {
        add_filter('wrap_tag', [$this, 'customTags']);
        add_filter('wrap_atts', [$this, 'customAtts']);

        parent::__construct();
    }

    public function customTags()
    {
        return 'ul';
    }

    public function customAtts()
    {
        return [
            'class' => 'list-khoahoc'
        ];
    }

    public function handle($query, $opts)
    {
        if ($query->have_posts()) {
            while ($query->have_posts()) {
                $query->the_post();
                $view = new View;
                // $view->setViewPath(__DIR__  . '/resources/views/');
                // $view->setCachePath(__DIR__  . '/storage/cache/');
                //var_dump($view->getViewPath());exit;

                $location = wp_get_post_terms(get_the_ID(), 'categoty_location');

                
                if (!empty($location)) {
                    
                    $locations = [];

                    foreach ($location as $key => $value) {
                        $locations[] = $value->name;
                    }

                    $locationStr = implode(', ', $locations);

                }

                $data = [
                    'id' => get_the_ID(),
                    'url' => get_permalink(),
                    'thumbnail' => wp_get_attachment_url(get_post_thumbnail_id()),
                    'title' => get_the_title(),
                    'excerpt' => get_the_excerpt(),
                    'day_start' => get_field('ngay_bat_dau', $post->ID),
                    'day_finish' => get_field('ngay_ket_thuc', $post->ID),
                    'status' => get_field('trang_thai', $post->ID),
                    'location' => $locationStr,
                    'thoi_luong' => get_field('thoi_luong', $post->ID),
                ];

                echo $view->render('daotao.daotao-item', $data);
            }

        }
    }
}
 
?>