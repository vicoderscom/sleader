<?php
namespace App\Shortcodes;
use NF\Abstracts\ShortCode;
use MSC\Filter;


class RenderFilters extends ShortCode
{
	// Doan nay tao mot object filter
	//add_shortcode('filter', 'renderFilters');

	public $name = 'filter';

	public function render($atts, $cont)
	{
		ob_start();

		//echo 'ok';exit;

		$filter = new Filter();

		$dataDate = [
			[
				'label' => __('Date Range', 'filter-khoahoc'),
				'value' => ''
			],

	        [
	            'label' => '2 ngày trước',
	            'value' => 2
	        ],
	        [
	            'label' => '20 ngày trước',
	            'value' => 20
	        ],
	        [
	            'label' => '60 ngày trước',
	            'value' => 60
	        ]
    	];

		$htmlDate = $filter->setFields([
	        [
	            'type' => 'select',
	            'source' => '',
	            'source_type' => 'date_range',
	            'data' => $dataDate,
	            'config' => [
	                'id' => 'filter_date_range',
	                'class' => 'date-range filter_kh',
	                'name' => 'date_range'
	            ],
	            'default' => ''
	        ]
    	])->render();

		echo $htmlDate;
		// Buoc 1:
		// Can lay data tu db cho field

		$tags = get_terms([
			'taxonomy' => 'categoris_topic',
			'hide_empty' => false,
		]);

		// Buoc 2:
		// Format lai cai array tags theo dung mau

		foreach ($tags as $key => $value) {
			$data[0] = [
				'label' => __('Topic','filter-khoahoc'),
				'value' => '',
			]; 

			$data[] = [
				'label' => $value->name,
				'value' => $value->term_id,
			];

			
		}

		$tagsHtml = $filter->setFields([
		    [
		        'type' => 'select',
		        'source' => 'categoris_topic',
		        'source_type' => 'tax',
		        'data' => $data,
		        'config' => [
		            'id' => 'chude-khoahoc',
		            'class' => 'filter_topic filter_kh',
		            'name' => 'categoris_topic',
		        ],
		        'default' => ''
		    ]
		])->render();

		echo $tagsHtml;



		$category = get_terms([
			'taxonomy' => 'khoahoc_category',
			'hide_empty' => false
		]);

		foreach ($category as $key => $value) {
			$dataCate[0] = [
				'label' => __('Category','filter-khoahoc'),
				'value' => ''
			];

			$dataCate[] = [
				'label' => $value->name,
				'value' => $value->term_id,
			];
		}


		$cateHtml = $filter->setFields([
			[
				'type' => 'select',
		        'source' => 'khoahoc_category',
		        'source_type' => 'tax',
		        'data' => $dataCate,
		        'config' => [
		            'id' => 'filter_district',
		            'class' => 'filter_category filter_kh',
		            'name' => 'khoahoc_category',
		        ],
			]
		])->render();

		echo $cateHtml;


		$location = get_terms([
			'taxonomy' => 'categoty_location',
			'hide_empty' => false,
		]);


		foreach ($location as $key => $value) {
			$dataLocation[0] = [
				'label' => __('Location','filter-khoahoc'),
				'value' => ''
			];

			$dataLocation[] = [
				'label' => $value->name,
				'value' => $value->term_id,
			];
		}


		$locationHtml = $filter->setFields([
			[
				'type' => 'select',
		        'source' => 'categoty_location',
		        'source_type' => 'tax',
		        'data' => $dataLocation,
		        'config' => [
		            'id' => 'filter_district',
		            'class' => 'filter_location filter_kh',
		            'name' => 'categoty_location',
		        ],
			]
		])->render();

		echo $locationHtml;

		$filterGo = '<div class="go_filter">go</div>';

		echo $filterGo;

		// =======================================
		// display post

		$number = wp_count_posts( $post_type = 'khoahoc' );

		// echo "<pre>";
		// var_dump($number);

		if(empty($_GET['_display'])){
			$view_number = '<div class="numner">' . __('Showing ','filter-khoahoc') .'1 - 10 '. __('of ','filter-khoahoc') .''. $number->publish .'' . __(' results','filter-khoahoc') .'</div>';
		}else{
			// $view_number = '<div class="numner">Hiển thị 1 - '. $_GET['_display'] .' trong tổng số '. $number->publish .' kết quả</div>';
			$view_number = '<div class="numner">' . __('Showing ','filter-khoahoc') .'1 - '. $_GET['_display'] .''. __(' of','filter-khoahoc') .''. $number->publish .'' . __(' results','filter-khoahoc') .'</div>';
		}



		echo $view_number;


		$display = [

	        [
	            'label' => '10',
	            'value' => 10
	        ],
	        [
	            'label' => '15',
	            'value' => 15
	        ],
	        [
	            'label' => '20',
	            'value' => 20
	        ],

	        [
	            'label' => '30',
	            'value' => 30
	        ],

    	];
    	// echo "<pre>";
    	// var_dump($display);

		$htmlDisplay = $filter->setFields([
	        [
	            'type' => 'select',
	            'source' => 'display',
	            'source_type' => '',
	            'data' => $display,
	            'config' => [
	                'id' => 'display-post',
	                'class' => 'date-range filter_kh',
	                'name' => 'display'
	            ],
	            'default' => ''
	        ]
    	])->render();

		echo '<div class="display_kh"><span class="view_khoahoc">' . __('Display','filter-khoahoc') .'</span><span class="option_display">'. $htmlDisplay .'</span></div>';


		// $count_posts = wp_count_posts( $post_type = 'khoahoc' );

		// var_dump($count_posts->publish);


		return ob_get_clean();
	}
}

?>