<?php

namespace App\RestApi;

/**
 * class handle relates post
 */
class PostApi extends \WP_REST_Controller
{
    /**
     * [$base The base to use in the API route]
     * @var string
     */
    protected $rest_base = 'highlight-post';

    /**
     * [$namespace namespace for routes API]
     * @var string
     */
    protected $namespace = 'wp/v2';

    /**
     * [__construct description]
     */
    public function __construct()
    {
        add_action('rest_api_init', [$this, 'register_routes']);
    }

    public function register_routes()
    {
        register_rest_route($this->namespace, "/{$this->rest_base}", [
            'methods'             => \WP_REST_Server::CREATABLE,
            'callback'            => [$this, 'get_items'],
            'permission_callback' => [$this, 'get_items_permissions_check'],
        ]);
    }

    /**
     * [get_items get collection of items ]
     *
     * @param WP_REST_Request $request Full data about the request
     *
     * @return WP_Error|WP_REST_Response
     */
    public function get_items($request)
    {
    	$params = $request->get_params();
        if (empty($params['number_page'])) {
        	$response_data = [
	        	'max_num_pages' => 0,
	        	'res_html' => ''
	        ];

	        $response = rest_ensure_response($response_data);
	        return $response;
        }

        add_action('pre_get_posts', function ($query) {
            // if ($query->query['post_type'] == 'news') {
                $query->set('posts_per_page', 3);
            // }
        });

        $news_post = new \WP_Query([
            'post_type'      => 'news',
            'posts_per_page' => 3,
            'paged'          => $params['number_page'],
        ]);

        $res_item = '';
        if(!empty($news_post)) {
	        foreach ($news_post->posts as $key => $item) {
	        	$res_item .= $this->prepare_item_for_response($item, $request);
	        }
        } else {
        	$res_item = '';
        }

        $response_data = [
        	'max_num_pages' => $news_post->max_num_pages,
        	'res_html' => $res_item
        ];

        $response = rest_ensure_response($response_data);
        return $response;
    }

    /**
     * Check if a given request has access to get items.
     *
     * @param WP_REST_Request $request Full data about the request.
     *
     * @return WP_Error|bool
     */
    public function get_items_permissions_check($request)
    {
        // return current_user_can('edit_posts');
        return true;
    }

    /**
     * Prepare the item for the REST response.
     *
     * @param stdClass        $item    WordPress representation of the item.
     * @param WP_REST_Request $request Request object.
     *
     * @return mixed
     */
    public function prepare_item_for_response($item, $request)
    {
    	$id_news = $item->ID;
        $img_news = wp_get_attachment_url(get_post_meta( $id_news, '_thumbnail_id', true ));
        $link_single_news = get_permalink($id_news);
        $title_page_news = get_the_title($id_news);
        $date_news = get_post_time( 'l, d-F-Y', false, $id_news, 'vi' );

        $item_str = '<li>';
        $item_str .= '<div class="row">';
        $item_str .= '<a href="' . $link_single_news .'">';
        $item_str .= '<div class="col-md-5 images_kh">';
        $item_str .= '<img style="background-image: url(' . $img_news .  ');" src="' . get_stylesheet_directory_uri() . '/resources/assets/images/home/post.png" />';
        $item_str .= '</div>';
        $item_str .= '<div class="col-md-7 single_home">';
        $item_str .= '<p class="title_kh_home">' . $title_page_news . '</p>';
        $item_str .= '<p class="time_kh">' . $date_news . '</p>';
        $item_str .= '</div>';
        $item_str .= '</a>';
        $item_str .= '</div>';
        $item_str .= '</li>';

        return $item_str;
    }
}
