<?php

namespace App\Providers;

use App\CustomPosts\TaxonomyTopic;
use App\CustomPosts\TaxonomyCategory;
use App\CustomPosts\TaxonomyLocation;
use App\CustomPosts\TaxonomyTraining;
use App\CustomPosts\TaxonomyAbout;
use Illuminate\Support\ServiceProvider;

class CustomTaxonomyServiceProvider extends ServiceProvider
{
    /**
     * [$listen description]
     * @var array
     */
    public $listen = [
        TaxonomyTopic::class,
        TaxonomyCategory::class,
        TaxonomyLocation::class,
        TaxonomyTraining::class,
        TaxonomyAbout::class,
    ];
    /**
     * [register description]
     * @return [type] [description]
     */
    public function register()
    {
        foreach ($this->listen as $class) {
            $this->resolveCustomPost($class);
        }
    }

    /**
     * [resolveCustomPost description]
     * @param  [type] $postType [description]
     * @return [type]           [description]
     */
    public function resolveCustomPost($postType)
    {
        return new $postType();
    }
}
