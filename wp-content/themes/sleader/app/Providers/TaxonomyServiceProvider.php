<?php

namespace App\Providers;
use App\CustomPosts\TaxonomyAbout;
use App\CustomPosts\TaxonomyCategory;
use App\CustomPosts\TaxonomyLocation;
use App\CustomPosts\TaxonomyTopic;
use App\CustomPosts\TaxonomyTraining;
use Illuminate\Support\ServiceProvider;

class TaxonomyServiceProvider extends ServiceProvider
{
    public $listen = [
        TaxonomyTopic::class,
        TaxonomyCategory::class,
        TaxonomyLocation::class,
        TaxonomyTraining::class,
        TaxonomyAbout::class,
    ];
    public function register()
    {
        foreach ($this->listen as $class) {
            $this->resolveShortCode($class);
        }
    }
    /**
     * Resolve a short_code instance from the class name.
     *
     * @param  string  $short_code
     * @return short_code instance
     */
    public function resolveShortCode($short_code)
    {
        return new $short_code();
    }
}