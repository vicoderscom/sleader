<?php

namespace App\Providers;

use App\RestApi\CatApi;
use App\RestApi\GocNhinApi;
use App\RestApi\PostApi;
use App\RestApi\PostApiScienve;
use Illuminate\Support\ServiceProvider;

class ApiServiceProvider extends ServiceProvider
{
    public $listen = [
        PostApi::class,
        PostApiScienve::class,
        GocNhinApi::class,
        CatApi::class,
    ];

    public function register()
    {
        foreach ($this->listen as $class) {
            $this->resolveApi($class);
        }
    }

    /**
     * Resolve a restapi instance from the class name.
     *
     * @param  string  $restapi
     * @return restapi instance
     */
    public function resolveApi($restapi)
    {
        return new $restapi();
    }
}
