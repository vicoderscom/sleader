<?php

namespace App\Providers;

use App\CustomPosts\Testimonials;
use App\CustomPosts\SlidePost;
use App\CustomPosts\About;
use App\CustomPosts\Course;
use App\CustomPosts\News;
use App\CustomPosts\Training;
use App\CustomPosts\EventSleader;
use App\CustomPosts\PointOfview;
use Illuminate\Support\ServiceProvider;

class CustomPostServiceProvider extends ServiceProvider
{
    /**
     * [$listen description]
     * @var array
     */
    public $listen = [
        About::class,
        Course::class,
        News::class,
        Training::class,
        EventSleader::class,
        PointOfview::class,
    ];

    /**
     * [register description]
     * @return [type] [description]
     */
    public function register()
    {
        foreach ($this->listen as $class) {
            $this->resolveCustomPost($class);
        }
    }

    /**
     * [resolveCustomPost description]
     * @param  [type] $postType [description]
     * @return [type]           [description]
     */
    public function resolveCustomPost($postType)
    {
        return new $postType();
    }
}
