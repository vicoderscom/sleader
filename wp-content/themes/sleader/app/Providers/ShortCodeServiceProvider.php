<?php

namespace App\Providers;

use App\Shortcodes\TestShortCode;
use App\Shortcodes\RenderFilters;
use App\Shortcodes\ListingDaoTao;
use App\Shortcodes\LichCacKhoaHoc;
use App\Shortcodes\ListSach;
use App\Shortcodes\DaoTao;
use App\Shortcodes\News;
use Illuminate\Support\ServiceProvider;

class ShortCodeServiceProvider extends ServiceProvider
{
    public $listen = [
        TestShortCode::class,
        RenderFilters::class,
        ListingDaoTao::class,
        LichCacKhoaHoc::class,
        ListSach::class,
        DaoTao::class,
        News::class,
    ];

    public function register()
    {
        //echo 'OK';exit;
        foreach ($this->listen as $class) {

            //var_dump($this->resolveShortCode($class));

            $this->resolveShortCode($class);
        }

        //exit;
    }

    /**
     * Resolve a short_code instance from the class name.
     *
     * @param  string  $short_code
     * @return short_code instance
     */
    public function resolveShortCode($short_code)
    {
        return new $short_code();
    }
}
