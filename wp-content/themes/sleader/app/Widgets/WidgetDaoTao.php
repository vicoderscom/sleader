<?php 
use MSC\Widget;

class WidgetDaoTao extends Widget
{
    public function __construct()
    {
        $widget = [
            'id' => 'test_widget',
            'label' => __('Submenu Đào Tạo'),
            'description' => __('This is test widget')
        ];

        $fields = [
            [   
                'label' => 'Images',
                'name' => 'images',
                'type' => 'upload',
            ],

            [   
                'label' => 'Nội Dung',
                'name' => 'single',
                'type' => 'text',
            ],
            
            [   
                'label' => 'Title Bài Viết',
                'name' => 'title',
                'type' => 'text',
            ],

            [   
                'label' => 'Slug Page',
                'name' => 'url',
                'type' => 'text',
            ],

        ];

        parent::__construct($widget, $fields);
    }
    
    public function handle($instance)
    {
        ?>
            <div class="row">
                <a href="<?php
                        if(ICL_LANGUAGE_CODE==en){
                            echo site_url('en'. '/' . $instance['url']);
                        }else{
                            echo site_url('vi'. '/' . $instance['url']);
                        }
                    ?>">
                    <div class="col-md-1"></div>
                    <div class="col-md-10">
                        <div class="submenu_daotao">
                            <div class="col-md-2 col-sm-2">
                                <img src="<?php echo $instance['images'] ?>" alt="">
                            </div>
                            <div class="col-md-10 col-sm-10">
                                <p class="title_sub"><?php echo $instance['title'] ?></p>
                                <p class="single_sub"><?php echo $instance['single'] ?></p>
                                <p class="view_daotao"><?php echo __('See details', 'widget_submenu'); ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                </a>
            </div>
        <?php
    }

}

new WidgetDaoTao;


// ==================================
//widget categoris

class widgetCategoris extends Widget
{
    public function __construct()
    {
        $widget = [
            'id' => 'widget_cate',
            'label' => __('Widget Catagoris Đào Tạo'),
            'description' => __('This is test widget')
        ];

        $fields = [
            [   
                'label' => 'Điền Shortcode',
                'name' => 'shortcode',
                'type' => 'text',
            ],
        ];
        parent::__construct($widget, $fields);
    }

    public function handle($instance)
    {   
        ?>
        <div class="pod-component horizontal component slick-mobile-only" style="background-color:#e8e8e3;">
            <div class="container">
                <?php echo do_shortcode($instance['shortcode']); ?>
            </div>
        </div>
        <?php
    }
}
new widgetCategoris;


// ==================================================

class WidgetDevelopment extends Widget
{
    
    public function __construct()
    {
        $widget = [
            'id' => 'widget_development',
            'label' => __('Widget Development Đào Tạo'),
            'description' => __('This is test widget')
        ];

        $fields = [
            [   
                'label' => 'images',
                'name' => 'background',
                'type' => 'upload',
            ],

            [
                'label' => 'title',
                'name' => 'title',
                'type' => 'text'
            ],

            [
                'label' => 'single',
                'name' => 'single',
                'type' => 'text'
            ],
        ];
        parent::__construct($widget, $fields);
    }

    public function handle($instance)
    {   
        ?>
        <div class="development">
            <div class="background" style="background:url(<?php echo $instance['background'] ?>);height: 340px;background-size: cover;background-position: 50%;width: 100%;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <p class="title_back"><?php echo $instance['title'] ?></p>
                            <p class="single_back"><?php echo $instance['single'] ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
}

new WidgetDevelopment;

// =========================================
// widget tư vấn và nghiên cứu

class SubItem extends Widget
{
    public function __construct()
    {
        $widget = [
            'id' => 'widget_subitem',
            'label' => __('Item Page Tư Vấn và Nghiên Cứu'),
            'description' => __('This is test widget')
        ];

        $fields = [
            [   
                'label' => 'images',
                'name' => 'avatar',
                'type' => 'upload',
            ],

            [
                'label' => 'url',
                'name' => 'url',
                'type' => 'text'
            ],


            [
                'label' => 'single',
                'name' => 'single',
                'type' => 'text'
            ],

            [
                'label' => 'title',
                'name' => 'title',
                'type' => 'text'
            ],
        ];
        parent::__construct($widget, $fields);
    }

    public function handle($instance)
    {
        ?>
            <div class="item_chienluoc col-md-3 col-sm-6 col-xs-12">
                <div class="item_tuvan">
                    <a href="<?php 
                        if(ICL_LANGUAGE_CODE==en){
                            echo site_url('en'). '/' . $instance['url'];
                        }else{
                            echo site_url('vi'). '/' . $instance['url'];
                        }
                        ?>" style="text-decoration: none;">
                        <div class="images">
                            <img src="<?php echo $instance['avatar'] ?>" alt="">
                        </div>

                        <div class="content_item">
                            <p class="title_item"><?php echo __( $instance['title'], 'tuvannghiencuu'); ?></p>

                            <p class="single_item">
                                <?php echo __( $instance['single'], 'tuvannghiencuu'); ?>
                            </p>

                            <p class="url_item"><?php echo __('Find out more', 'tuvannghiencuu'); ?></p>
                        </div>
                    </a>
                </div>
            </div>
        <?php
    }
}

new SubItem;

// ===================================

//widget dao tao content


class ContentDaoTao extends Widget
{
    public function __construct()
    {
        $widget = [
            'id' => 'widget_contentdt',
            'label' => __('Why learn with us?'),
            'description' => __('This is test widget')
        ];

        $fields = [
            [   
                'label' => 'images',
                'name' => 'avatar',
                'type' => 'upload',
            ],

            [
                'label' => 'single',
                'name' => 'single',
                'type' => 'textarea'
            ],

            
            [
                'label' => 'title',
                'name' => 'title',
                'type' => 'text'
            ],
        ];
        parent::__construct($widget, $fields);
    }

    public function handle($instance)
    {   
        $sidebars = wp_get_sidebars_widgets();
        ?>
            <div class="learn">
                <div class="container">
                    <div class="noidung">
                        <ul>
                            <li class="row">
                                <div class="col-md-2 col-sm-3 images_learn">
                                    <img src="<?php echo $instance['avatar'] ?>" alt="Foundations of Directorship">
                                </div>
                                <div class="col-md-10 col-sm-9 single_learn">
                                    <p class="title_post"><?php echo $instance['title'] ?></p>
                                    <p class="single_post"><?php echo $instance['single'] ?></p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        <?php
    }
}

new ContentDaoTao;

// ==================================

class ViewAllPage extends Widget
{
    public function __construct()
    {
        $widget = [
            'id' => 'view_all',
            'label' => __('View All Page'),
            'description' => __('This is test widget')
        ];

        $fields = [
            [   
                'label' => 'url_button',
                'name' => 'url_button',
                'type' => 'text',
            ],

            [   
                'label' => 'title_button',
                'name' => 'title_button',
                'type' => 'text',
            ],

            [
                'label' => 'single',
                'name' => 'single',
                'type' => 'text'
            ],

            
            [
                'label' => 'title',
                'name' => 'title',
                'type' => 'text'
            ],
        ];
        parent::__construct($widget, $fields);
    }

    public function handle($instance)
    {
        ?>
            <div class="list_topic">
                <div class="container">
                    <p class="tieude"><?php echo $instance['title'] ?></p>
                    <p class="about_topic"><?php echo $instance['single'] ?></p>

                    <button class="url_topic">
                        <a href="<?php
                            if(ICL_LANGUAGE_CODE==en){
                                echo site_url('en'. '/' . $instance['url_button']);
                            }else{
                                echo site_url('vi'. '/' . $instance['url_button']);
                            } ?>"><?php echo $instance['title_button'] ?></a>
                    </button>
                </div>
            </div>
        <?php
    }
}

new ViewAllPage;


// ===========================================


class SubItemChienLuoc extends Widget
{
    public function __construct()
    {
        $widget = [
            'id' => 'widget_chienluoc',
            'label' => __('Item Tư Vấn Chiến Lược'),
            'description' => __('This is test widget')
        ];

        $fields = [
            [   
                'label' => 'images',
                'name' => 'avatar',
                'type' => 'upload',
            ],

            [
                'label' => 'single',
                'name' => 'single',
                'type' => 'text'
            ],

            [
                'label' => 'title',
                'name' => 'title',
                'type' => 'text'
            ],
        ];
        parent::__construct($widget, $fields);
    }

    public function handle($instance)
    {
        ?>
            <div class="col-md-4 item_supper">
                <div class="item_tuvan">
                    <div class="images">
                        <img src="<?php echo $instance['avatar'] ?>" alt="">
                    </div>

                    <div class="content_item">
                        <p class="title_item"><?php echo $instance['title'] ?></p>

                        <p class="single_item">
                            <?php echo $instance['single'] ?>
                        </p>
                    </div>
                </div>
            </div>
        <?php
    }
}

new SubItemChienLuoc;

// ===================================


// ===========================================


class AuthorTuVan extends Widget
{
    public function __construct()
    {
        $widget = [
            'id' => 'widget_author',
            'label' => __('Author For Tư Vấn'),
            'description' => __('This is test widget')
        ];

        $fields = [
            [   
                'label' => 'images',
                'name' => 'avatar',
                'type' => 'upload',
            ],

            [
                'label' => 'single',
                'name' => 'single',
                'type' => 'text'
            ],

            [
                'label' => 'address',
                'name' => 'address',
                'type' => 'text'
            ],

            [
                'label' => 'name',
                'name' => 'name',
                'type' => 'text'
            ],
        ];
        parent::__construct($widget, $fields);
    }

    public function handle($instance)
    {
        ?>
            <div class="author container">
                <div class="row">
                    <div class="col-sm-6 col-md-4 col-md-offset-1 images_author">
                        <img style="background-image:url(<?php echo $instance['avatar'] ?>);" src="<?php echo get_stylesheet_directory_uri() ?>/resources/assets/images/tuvan/tacgia.png" alt="">
                    </div>

                    <div class="col-sm-6 col-md-8 noidung_author">
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/resources/assets/images/tuvan/icon.png" alt="">
                        <p class="single_author">
                            <?php echo $instance['single'] ?>
                        </p>
                        <p class="name_author">
                            <strong><?php echo $instance['name'] ?>,</strong>
                            <?php echo $instance['address'] ?>
                        </p>
                    </div>
                </div>
            </div>
        <?php
    }
}

new AuthorTuVan;

// ==================================

class LienHe extends Widget
{
    public function __construct()
    {
        $widget = [
            'id' => 'widget_lienhe',
            'label' => __('Page Liên Hệ'),
            'description' => __('This is test widget')
        ];

        $fields = [
            [   
                'label' => 'Shortcode Maps',
                'name' => 'shortcodemaps',
                'type' => 'text',
            ],

            [   
                'label' => 'Shortcode',
                'name' => 'shortcode',
                'type' => 'text',
            ],

            [   
                'label' => 'Địa Chỉ',
                'name' => 'diahi',
                'type' => 'text',
            ],

            [
                'label' => 'Email',
                'name' => 'email',
                'type' => 'text'
            ],

            [
                'label' => 'Phone',
                'name' => 'phone',
                'type' => 'text'
            ],

            [
                'label' => 'title',
                'name' => 'title',
                'type' => 'text'
            ],
        ];
        parent::__construct($widget, $fields);
    }

    public function handle($instance)
    {
        ?>
            <p class="title_page"><?php echo $instance['title'] ?></p>
        
            <p class="thongtin sdt"><i class="fa fa-phone" aria-hidden="true"></i> <?php echo $instance['phone'] ?></p>
            <p class="thongtin email"><i class="fa fa-envelope" aria-hidden="true"></i> <?php echo $instance['email'] ?></p>
            <p class="thongtin diachi"><i class="fa fa-map-marker" aria-hidden="true"></i> <?php echo $instance['diahi'] ?></p>

            <div class="form_lienhe">
                <?php
                    echo do_shortcode($instance["shortcode"]);
                ?>
            </div>

            <div class="maps">
                <?php
                    echo do_shortcode($instance["shortcodemaps"]);
                ?>
            </div>
        <?php
    }
}

new LienHe;

// ====================================


class IconHeader extends Widget
{
    public function __construct()
    {
        $widget = [
            'id' => 'icon_header',
            'label' => __('Icon Header'),
            'description' => __('This is test widget')
        ];

        $fields = [
            [   
                'label' => 'Url Youtube',
                'name' => 'url_youtube',
                'type' => 'text',
            ],

            [
                'label' => 'Url Facebook',
                'name' => 'url_facebook',
                'type' => 'text'
            ],

            [
                'label' => 'Url Twitter',
                'name' => 'url_twitter',
                'type' => 'text'
            ],

            [
                'label' => 'Url Linkedin',
                'name' => 'url_linkedin',
                'type' => 'text'
            ],
        ];
        parent::__construct($widget, $fields);
    }

    public function handle($instance)
    {
        ?>
            <li> <a href="<?php echo $instance['url_linkedin'] ?>"><i class='fa fa-linkedin' aria-hidden='true'></i></a></li>
            <li> <a href="<?php echo $instance['url_twitter'] ?>"><i class='fa fa-twitter' aria-hidden='true'></i></a>
            </li>
            <li> <a href="<?php echo $instance['url_facebook'] ?>"><i class='fa fa-facebook-official' aria-hidden='true'></i></a>
            </li>
            <li> <a href="<?php echo $instance['url_youtube'] ?>"><i class='fa fa-youtube-play' aria-hidden='true'></i></a>
            </li>
        <?php
    }
}

new IconHeader;

/*
====================================================*/

// ===========================================


class Logo extends Widget
{
    public function __construct()
    {
        $widget = [
            'id' => 'widget_logo',
            'label' => __('Logo'),
            'description' => __('This is test widget')
        ];

        $fields = [
            [   
                'label' => 'text_slogan',
                'name' => 'text_slogan',
                'type' => 'text',
            ],
            [   
                'label' => 'text_logo',
                'name' => 'text_logo',
                'type' => 'text',
            ],

        ];
        parent::__construct($widget, $fields);
    }

    public function handle($instance)
    {
        ?>
            <div class="text_logo">
                <p><?php echo $instance['text_logo'] ?></p>
            </div>

            <div class="slogan">
                <p><?php echo $instance['text_slogan'] ?></p>
            </div>
        <?php
    }
}

new Logo;

// ==============================

class Hotline extends Widget
{
    public function __construct()
    {
        $widget = [
            'id' => 'widget_Hotline',
            'label' => __('Hotline'),
            'description' => __('This is test widget')
        ];

        $fields = [
            [   
                'label' => 'number_sdt',
                'name' => 'number_sdt',
                'type' => 'text',
            ],

            [
                'label' => 'Name',
                'name' => 'name',
                'type' => 'text'
            ],
        ];
        parent::__construct($widget, $fields);
    }

    public function handle($instance)
    {
        ?>
            <div>
                <div class="phone <?php if(ICL_LANGUAGE_CODE==en){ echo"phone_en"; } ?>">
                    <a href="tel:<?php echo $instance['number_sdt'] ?>">
                        <span><i class="fa fa-phone" aria-hidden="true"></i></span>
                        <span> <?php echo $instance['name'] ?>: <?php echo $instance['number_sdt'] ?></span>
                    </a>
                </div>

                <div class="phone_moblie animated tada infinite">
                    <a href="tel:<?php echo $instance['number_sdt'] ?>">
                        <span><i class="fa fa-phone" aria-hidden="true"></i></span>
                    </a>
                </div>
            </div>
        <?php
    }
}

new Hotline;



// ================footer

class FooterInfo extends Widget
{
    public function __construct()
    {
        $widget = [
            'id' => 'footer_info',
            'label' => __('Footer info và maps'),
            'description' => __('This is test widget')
        ];

        $fields = [
            [   
                'label' => 'images',
                'name' => 'maps',
                'type' => 'upload',
            ],

            [
                'label' => 'Tên viện',
                'name' => 'name_vien',
                'type' => 'text'
            ],

            [
                'label' => 'Địa chỉ',
                'name' => 'diachi',
                'type' => 'textarea'
            ],

            [
                'label' => 'Email',
                'name' => 'email',
                'type' => 'text'
            ],

            [
                'label' => 'Hotline',
                'name' => 'hotline',
                'type' => 'text'
            ],
        ];
        parent::__construct($widget, $fields);
    }

    public function handle($instance)
    {
        ?>
            <div class="container">
                <div class="row">
                    <div class="footer_info col-md-6">
                        <p class="name_vien"><?php echo $instance['name_vien'] ?></p>
                        <p class="diachi"><?php echo __( 'Address', 'footer'); ?>: <?php echo $instance['diachi'] ?></p>
                        <P class="eamil">Email: <?php echo $instance['email'] ?></P>
                        <p class="hotline">Hotline: <?php echo $instance['hotline'] ?></p>
                    </div>

                    <div class="col-md-6 maps_footer">
                        <img style="background-image: url(<?php echo $instance['maps'] ?>); " src="<?php echo get_stylesheet_directory_uri() ?>/resources/assets/images/home/maps.png">
                    </div>            
                </div>
            </div>
        <?php
    }
}

new FooterInfo;

// =====================slider

class SliderHome extends Widget
{
    public function __construct()
    {
        $widget = [
            'id' => 'slider',
            'label' => __('Slider Homepage'),
            'description' => __('This is test widget')
        ];

        $fields = [
            [
                'label' => 'Shortcode',
                'name' => 'shortcode',
                'type' => 'text'
            ],
        ];
        parent::__construct($widget, $fields);
    }

    public function handle($instance)
    {
        ?>
            <div class="container-fluide">
                <?php echo do_shortcode($instance['shortcode']); ?>
            </div>
        <?php
    }
}

new SliderHome;


?>