<?php

namespace App\CustomPosts;

use NF\Abstracts\CustomPost;

class EventSleader extends CustomPost
{
    public $type = 'eventsleader';

    public $single = 'eventsleader';

    public $plural = 'Event Sleader';

    public $args = ['menu_icon' => 'dashicons-format-status'];

}
