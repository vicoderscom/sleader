<?php

namespace App\CustomPosts;
use MSC\Tax;

class TaxonomyCategory extends Tax
{
    // public $post_type = 'khoahoc';

    // public $taxonomy_type = 'khoahoc_category';

    // public $args = [
    // 	'label' => 'Categoris',
    // 	'menu_icon' => 'dashicons-format-status'
    // ];

    public function __construct()
    {
    	$config = [
			'slug' => 'khoahoc_category',
			'single' => 'Category',
			'plural' => 'Category'
		];

		$postType = 'khoahoc';

		$args = [
			'menu_icon' => 'dashicons-format-status'
		];

		parent::__construct($config, $postType, $args);
    }
}
