<?php

namespace App\CustomPosts;
use MSC\Tax;

class TaxonomyTraining extends Tax
{
    public $post_type = 'training';

    public $taxonomy_type = 'categoris_training';

    public $args = ['menu_icon' => 'dashicons-format-status'];

    public function __construct()
    {
    	$config = [
			'slug' => 'categoris_training',
			'single' => 'Category',
			'plural' => 'Category'
		];

		$postType = 'training';

		$args = [
			'menu_icon' => 'dashicons-format-status'
		];

		parent::__construct($config, $postType, $args);
    }
}
