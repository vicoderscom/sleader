<?php

namespace App\CustomPosts;

use NF\Abstracts\CustomPost;

class About extends CustomPost
{
    public $type = 'about';

    public $single = 'about';

    public $plural = 'About';

    public $args = ['menu_icon' => 'dashicons-format-status'];

}
