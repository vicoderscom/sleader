<?php

namespace App\CustomPosts;

use NF\Abstracts\CustomPost;

class Course extends CustomPost
{
    public $type = 'khoahoc';

    public $single = 'Khóa Học';

    public $plural = 'Khóa Học';

    public $args = [
    	'menu_icon' => 'dashicons-format-status'
    ];
}
