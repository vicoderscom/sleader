<?php

namespace App\CustomPosts;
use MSC\Tax;

class TaxonomyTopic extends Tax
{
    // public $post_type = 'khoahoc';

    // public $taxonomy_type = 'categoris_topic';

    // public $args = [
    // 	'label' => 'Topic',
    // 	'menu_icon' => 'dashicons-format-status'
    // ];

    public function __construct()
    {
    	$config = [
			'slug' => 'categoris_topic',
			'single' => 'Topic',
			'plural' => 'Topic'
		];

		$postType = 'khoahoc';

		$args = [
			'menu_icon' => 'dashicons-format-status'
		];

		parent::__construct($config, $postType, $args);
    }
}
