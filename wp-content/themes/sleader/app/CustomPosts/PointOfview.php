<?php

namespace App\CustomPosts;

use NF\Abstracts\CustomPost;

class PointOfview extends CustomPost
{
    public $type = 'point-of-view';

    public $single = 'point-of-view';

    public $plural = 'Point of view';

    public $args = ['menu_icon' => 'dashicons-format-status'];

}
