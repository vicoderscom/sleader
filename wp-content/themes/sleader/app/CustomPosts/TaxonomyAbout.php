<?php

namespace App\CustomPosts;
use MSC\Tax;

class TaxonomyAbout extends Tax
{
    // public $post_type = 'about';

    // public $taxonomy_type = 'categoris_about';

    // public $args = ['menu_icon' => 'dashicons-format-status'];

    public function __construct()
    {
    	$config = [
			'slug' => 'categoris_about',
			'single' => 'Categoris About',
			'plural' => 'Categoris About'
		];

		$postType = 'about';

		$args = [
			'menu_icon' => 'dashicons-format-status'
		];

		parent::__construct($config, $postType, $args);
    }
}
