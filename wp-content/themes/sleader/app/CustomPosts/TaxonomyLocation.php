<?php

namespace App\CustomPosts;
use MSC\Tax;

class TaxonomyLocation extends Tax
{
    // public $post_type = 'khoahoc';

    // public $taxonomy_type = 'categoty_location';

    // public $args = [
    // 	'label' => 'Location',
    // 	'menu_icon' => 'dashicons-format-status'
    // ];

    public function __construct()
    {
    	$config = [
			'slug' => 'categoty_location',
			'single' => 'Location',
			'plural' => 'Location'
		];

		$postType = 'khoahoc';

		$args = [
			'menu_icon' => 'dashicons-format-status'
		];

		parent::__construct($config, $postType, $args);
    }
}
