<?php

namespace App\CustomPosts;

use NF\Abstracts\CustomPost;

class News extends CustomPost
{
    public $type = 'news';

    public $single = 'news';

    public $plural = 'News';

    public $args = ['menu_icon' => 'dashicons-format-status'];

}
