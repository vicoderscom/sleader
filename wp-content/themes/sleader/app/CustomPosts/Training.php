<?php

namespace App\CustomPosts;

use NF\Abstracts\CustomPost;

class Training extends CustomPost
{
    public $type = 'training';

    public $single = 'training';

    public $plural = 'Training';

    public $args = ['menu_icon' => 'dashicons-format-status'];

}
